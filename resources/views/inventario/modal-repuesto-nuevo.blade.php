<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-repuesto-nuevo">

	<div class="modal-dialog">

		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>

				<h2 class="text-center text-primary">Repuesto Nuevo</h2>
				<div id="success-repuesto">
					

				</div>
			</div>

			<div class="modal-body">

				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">

							<label for="descripcion">Descripcion</label>
							<input type="text" id="descripcion" name="descripcion" class="form-control" value="" >

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">

							<label for="categoria">Categoria</label>
							<select id="categoria" name="categoria" class="form-control" value="">
								@foreach($categorias as $categoria)
								<option value="{{$categoria->Id}}">{{$categoria->Descripcion}}</option>
								@endforeach
							</select>

						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">

							<label for="um">Unidad de Medida</label>
							<select id="um" name="um" class="form-control">
								@foreach($unidades_medida as $unidad_medida)
								<option value="{{$unidad_medida->Id}}">{{$unidad_medida->Descripcion}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">

							<label for="proveedor">Proveedores</label>
							<select id="proveedor" name="proveedor" class="form-control">
								@foreach($lista_proveedores as $proveedor)
								<option value="{{$proveedor->Ruc}}">{{$proveedor->RazonSocial}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">

							<label for="cantidad">Cantidad</label>
							<input type="number" id="cantidad" name="cantidad" class="form-control"  >

						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">

							<label for="precio_venta">Precio de Venta</label>
							<input type="number" id="precio_venta" step="0.01" value="0.00" name="precio_venta" class="form-control" placeholder="Precio de Venta" >
						</div>
					</div>
				</div>
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" id="btn-guardar-repuesto" class="btn btn-primary">Guardar</button>
			</div>
		</div>

	</div>

	

</div>

<meta name="_token" content="{!! csrf_token() !!}" />

