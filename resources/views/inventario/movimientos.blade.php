@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-12">
		<h1 class="text-primary">{{$repuesto->Id}} - {{$repuesto->Descripcion}}</h1>
		
	</div>
</div>



<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-hover table-bordered table-striped">
				<thead>
					<tr>
						<th scope="col" class="col-lg-2">Fecha</th>
						<th scope="col" class="col-lg-2">Movimiento</th>
						<th scope="col" class="col-lg-2">Cantidad</th>
						<th scope="col" class="col-lg-2">Stock</th>
					</tr>
					
				</thead>
				@foreach ($movimientos as $movimiento)
				<tr>
					<td>{{ $movimiento->Fecha }}</td>
					<td>{{ $movimiento->TipoMovimiento }}</td>
					<td>{{ $movimiento->Cantidad }}</td>
					<td>{{ $movimiento->Stock }}</td>
				</tr>

				@endforeach

				
			</table>
		</div>
		{{$movimientos->render()}}
	</div>
</div>

@endsection