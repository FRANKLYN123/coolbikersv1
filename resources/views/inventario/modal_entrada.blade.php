<div class="modal fade" aria-hidden="true" role="dialog" tabindex="-1" id="modal_entrada_inventario">

	<div class="modal-dialog modal-sm">

		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h2 class="text-center text-primary">Entrada</h2>
				
				
				<div id="campo_respuesta_entrada_inventario">
				</div>

				
			</div>

			<div class="modal-body">
				<form id="formulario_cliente_nuevo_ajax">
					
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="repuesto">Repuesto</label>
								<select id="repuesto_entrada" name="repuesto" class="form-control" value="">
									@foreach ($lista_repuestos as $repuesto)
									<option value="{{$repuesto->Id}}">{{$repuesto->Descripcion}}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="repuesto">Proveedor</label>
								<select id="proveedor_entrada" name="proveedor" class="form-control">
									@foreach ($lista_proveedores as $proveedor)
									<option value="{{$proveedor->Ruc}}">{{$proveedor->RazonSocial}}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label for="cantidad_entrada">Cantidad</label>
								<input type="number" id="cantidad_entrada" name="cantidad" class="form-control" value="0" required="">
							</div>
						</div>
						<div class="col-lg-8">
							<div class="form-group">
								<label for="precio_unitario_entrada">Precio Unitario ( S/ )</label>
								<input type="number" id="precio_unitario_entrada" name="precio_unitario" class="form-control" value="0.00" required>
							</div>
						</div>
					</div>

					
				</form>
				
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-danger" id="boton_cerrar_modal_cliente" data-dismiss="modal">Cerrar</button>
				<button type="submit" id="boton_guardar_entrada" class="btn btn-primary">Guardar</button>
			</div>
		</div>

	</div>

	

</div>



