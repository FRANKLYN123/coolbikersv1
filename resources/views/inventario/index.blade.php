@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-12">
		<h1 class="text-primary">INVENTARIO</h1>
		
	</div>

	@if (count($errors)>0)
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
	@endif
</div>

<div class="row">

	<div class="col-lg-4">
		<div class="input-group">
			<input type="text" class="form-control" name="searchText" placeholder="" value="{{$searchText}}">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-primary">Buscar</button>
			</span>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="form-group">
			<a data-target="#modal-repuesto-nuevo" data-toggle="modal">
				<button class="btn btn-success">Repuesto Nuevo</button>
			</a>
			@include('inventario.modal-repuesto-nuevo')
			<a data-target="#modal_entrada_inventario" data-toggle="modal">
				<button class="btn btn-success">Entrada</button>
			</a>
			@include('inventario.modal_entrada')
			<a data-target="#modal_salida_inventario" data-toggle="modal">
				<button class="btn btn-success">Salida</button>
			</a>
			@include('inventario.modal_salida')
			
		</div>
	</div>

</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-hover table-bordered table-striped">
				<thead>
					<tr>
						<th scope="col" class="col-lg-1">Id</th>
						<th scope="col" class="col-lg-5 tabla_columna_descripcion">Descripcion</th>
						<th scope="col" class="col-lg-1">Categoria</th>
						<th scope="col" class="col-lg-1">U.M.</th>
						<th scope="col" class="col-lg-2">Precio de Venta</th>
						<th scope="col" class="col-lg-2">Stock</th>

						<th>Ver Movimientos</th>
					</tr>
					
				</thead>
				@foreach ($repuestos as $repuesto)
				<tr>
					<th scope="row">{{ $repuesto->Id }}</th>
					<td class="tabla_columna_descripcion">{{ $repuesto->Descripcion }}</td>
					<td>{{ $repuesto->Categoria }}</td>
					<td>{{ $repuesto->UM }}</td>
					<td>S/ {{ $repuesto->PrecioVenta }}</td>
					<td>{{ $repuesto->Stock }}</td>
					<td>
						<a href="{{URL::action('InventarioController@show',$repuesto->Id)}}"><button class="btn btn-primary">Ver Movimientos</button></a>						
					</td>
				</tr>

				@endforeach

				
			</table>
		</div>
		{{$repuestos->render()}}
	</div>
</div>

@endsection