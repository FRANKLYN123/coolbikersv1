<div class="modal fade" aria-hidden="true" role="dialog" tabindex="-1" id="modal_salida_inventario">

	<div class="modal-dialog modal-sm">

		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h2 class="text-center text-primary">Salida</h2>
				
				
				<div id="campo_respuesta_salida_inventario">
				</div>

				
			</div>

			<div class="modal-body">
				<form id="formulario_cliente_nuevo_ajax">
					
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="repuesto_salida">Repuesto</label>
								<select id="repuesto_salida" name="repuesto_salida" class="form-control" value="">
									@foreach ($lista_repuestos as $repuesto)
									<option value="{{$repuesto->Id}}">{{$repuesto->Descripcion}}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label class="text-muted" for="cliente_salida">Cliente (Opcional)</label>
								<select id="cliente_salida" name="cliente_salida" class="form-control">
									@foreach ($lista_clientes as $cliente)
									<option value="{{$cliente->Id}}">{{$cliente->Nombres}}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="motivo_salida">Motivo</label>
								<textarea id="motivo_salida" name="motivo_salida" class="form-control" rows="3" ></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label for="cantidad_salida">Cantidad</label>
								<input type="number" id="cantidad_salida" name="cantidad_salida" class="form-control" value="0" required="">
							</div>
						</div>
						<div class="col-lg-8">
							<div class="form-group">
								<label for="precio_unitario_salida">Precio Unitario ( S/ )</label>
								<input type="number" id="precio_unitario_salida" name="precio_unitario_salida" class="form-control" value="0.00" required>
							</div>
						</div>
					</div>

					
				</form>
				
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
				<button type="submit" id="boton_guardar_salida_venta_directa" class="btn btn-primary">Guardar</button>
			</div>
		</div>

	</div>

	

</div>



