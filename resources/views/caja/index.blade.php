@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-12">
		<h1 class="text-primary">CAJA</h1>
		
	</div>
</div>


<div class="row">

	<div class="col-lg-4">
		<div class="input-group">
			<input type="text" class="form-control" name="searchText" placeholder="" value="{{$searchText}}">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-primary">Buscar</button>
			</span>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="form-group">
			<a href="" data-target="#modal_nuevo_movimiento" data-toggle="modal">
				<button class="btn btn-success">Ingresar Movimiento</button>
			</a>
			@include('caja.modal_create')

		</div>
	</div>

</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-hover table-bordered table-striped">
				<thead>
					<tr>
						<th scope="col" class="col-lg-2">Fecha</th>
						<th scope="col" class="col-lg-3 tabla_columna_descripcion">Descripcion</th>
						<th scope="col" class="col-lg-1">Tipo</th>
						<th scope="col" class="col-lg-1">Monto</th>
						<th scope="col" class="col-lg-1">Saldo</th>
					</tr>
				</thead>
				@foreach ($movimientos as $movimiento)
				<tr>
					<td>{{ $movimiento->FechaMovimiento }}</td>
					<td class="tabla_columna_descripcion">{{ $movimiento->Descripcion }}</td>
					<td>{{ $movimiento->Movimiento }}</td>
					<td>S/ {{ $movimiento->Monto }}</td>
					<td>S/ {{ $movimiento->Saldo }}</td>
					
				</tr>
				@endforeach
			</table>
		</div>
		{{$movimientos->render()}}
	</div>
</div>



@endsection