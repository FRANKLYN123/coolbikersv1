<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal_nuevo_movimiento">

	<div class="modal-dialog">

		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h2 class="text-center text-primary">Movimiento Nuevo</h2>
				
				
				<div id="campo_respuesta_caja">
				</div>

				
			</div>

			<div class="modal-body">
				<form id="formulario_movimiento_nuevo">
					
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="descripcion_movimiento">Descripcion</label>
								<textarea id="descripcion" class="form-control" rows="3" ></textarea>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-lg-4">
							<label for="monto">Tipo</label>
							<select id="movimiento" class="form-control">
								<option value="entrada">Entrada</option>
								<option value="salida">Salida</option>
							</select>
						</div>
						<div class="form-group col-lg-4">
							<label for="monto">Monto</label>
							<input type="number" id="monto" name="monto" class="form-control" value="0.00" required>
						</div>

						<div class="form-group col-lg-4">
							<label>Fecha</label>
							<input type="date" name="fecha_movimiento" id="fecha_movimiento" class="form-control" >
						</div>
					</div>
					
				</form>
				
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="boton_cerrar_modal_cliente" data-dismiss="modal">Cerrar</button>
				<button type="submit" id="boton_guardar_movimiento" class="btn btn-primary">Guardar</button>
			</div>
		</div>

	</div>

	

</div>



