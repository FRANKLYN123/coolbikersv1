@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-6">
		<h3>Nuvo Movimiento</h3>

		{!! Form::open(array('url'=>'caja','method'=>'POST','autocomplete'=>'off')) !!}

		{{Form::token()}}

		<div class="form-group">
			<label for="descripcion">Descripcion</label>
			<input type="text" name="descripcion" class="form-control" placeholder="Descripcion" value="{{old('descripcion')}}" required>
		</div>
		

		<div class="form-group col-lg-4">
			<select id="tipo" class="form-control">
				<option value="entrada">Entrada</option>
				<option value="salida">Salida</option>
			</select>
		</div>
		<div class="col-lg-8"></div>
		<div class="form-group">
			<button class="btn btn-primary" type="submit">Guardar</button>
			<a href="{{url('almacen/categoria')}}"><button class="btn btn-danger">Cancelar</button></a>

			{!! Form::close() !!}

		</div>
	</div>
</div>

@endsection