<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-cliente-nuevo">

	<div class="modal-dialog">

		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h2 class="text-center text-primary">Cliente Nuevo</h2>
				
				
				<div id="campo_respuesta_cliente">
				</div>
			
				
			</div>

			<div class="modal-body">
				<form id="formulario_cliente_nuevo_ajax">
					
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">

								<label for="numero_documento">DNI / RUC</label>
								<input type="number" id="numero_documento" name="numero_documento" class="form-control" value="" required>

							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">

								<label for="telefono">Telefono</label>
								<input type="number" id="telefono" name="telefono" class="form-control">
							</div>
						</div>
					</div>

					<div class="form-group">

						<label for="cliente">Nombres / Razon Social</label>
						<input type="text" id="nombre_cliente" name="nombre_cliente" class="form-control" value="" required>

					</div>
					<div class="form-group">

						<label for="direccion">Direccion</label>
						<input type="text" id="direccion" name="direccion" class="form-control">

					</div>
					<div class="form-group">

						<label for="correo">Correo</label>
						<input type="email" id="correo" name="correo" class="form-control" placeholder="example@mail.com" >

					</div>
				</form>
				
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="boton_cerrar_modal_cliente" data-dismiss="modal">Cerrar</button>
				<button type="submit" id="btn-guardar-cliente" value="add" class="btn btn-primary">Guardar</button>
			</div>
		</div>

	</div>

	

</div>



