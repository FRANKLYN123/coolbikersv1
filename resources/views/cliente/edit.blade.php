@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Editar Cliente</h3>
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
		@endif

		{!! Form::model($cliente,['method'=>'PATCH','route'=>['cliente.update',$cliente->Id]]) !!}

		{{Form::token()}}

		<div class="form-group">
			<label for="numero_documento">Nº Documento</label>
			<input type="number" name="numero_documento" class="form-control"  placeholder="DNI o RUC" value="{{$cliente->Id}}" required disabled>
		</div>
		<div class="form-group">
			<label for="cliente">Nombres</label>
			<input type="text" name="cliente" class="form-control" placeholder="Nombres" value="{{$cliente->Nombres}}" required>
		</div>

		<div class="form-group">
			<label for="direccion">Direccion</label>
			<input type="text" name="direccion" class="form-control" placeholder="Direccion" value="{{$cliente->Direccion}}">
		</div>

		<div class="form-group">
			<label for="correo">Correo</label>
			<input type="email" name="correo" class="form-control" placeholder="Correo" value="{{$cliente->Correo}}">
		</div>
		<div class="form-group">
			<label for="telefono">Telefono</label>
			<input type="number" name="telefono" class="form-control" placeholder="Telefono" value="{{$cliente->Telefono}}">
		</div>

		<div class="form-group">
			<button class="btn btn-primary" type="submit">Guardar</button>
			<button class="btn btn-danger" type="reset">Descartar</button>

		</div>
		{!! Form::close() !!}

	</div>
</div>

@endsection