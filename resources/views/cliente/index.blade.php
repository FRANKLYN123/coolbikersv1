@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h3>
			Clientes
			<a href="" data-target="#modal-cliente-nuevo" data-toggle="modal">
				<button class="btn btn-success">Nuevo Cliente</button>
			</a>

		</h3>
		@include('cliente.modal_create')
		@include('cliente.search')
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>N° Documento</th>
					<th>Tipo de Documento</th>
					<th>Nombres</th>
					<th>Direccion</th>
					<th>Correo</th>
					<th>Telefono</th>
					<th>Opciones</th>
					
				</thead>
				@foreach ($clientes as $cliente)
				<tr>
					<td>{{ $cliente->Id }}</td>
					<td>{{ $cliente->TipoDocumento }}</td>
					<td>{{ $cliente->Nombres }}</td>
					<td>{{ $cliente->Direccion }}</td>
					<td>{{ $cliente->Correo }}</td>
					<td>{{ $cliente->Telefono }}</td>
					<td>
						<a href="{{URL::action('ClienteController@edit',$cliente->Id)}}"><button class="btn btn-primary">Editar</button></a>
						<a href="" data-target="#modal-delete-{{$cliente->Id}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
					</td>
				</tr>
				@include('cliente.modal_remove')
				@endforeach
			</table>
		</div>
		{{$clientes->render()}}
	</div>
</div>

@endsection