@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		

		<select class="js-example-basic-single" name="state">
			<option value="AL">Alabama</option>
			<option value="WY">Wyoming</option>
		</select>

		<select class="js-example-basic-multiple" name="states[]" multiple="multiple">
			<option value="AL">Alabama</option>
			<option value="WY">Wyoming</option>
		</select>
	</div>
</div>
@endsection
