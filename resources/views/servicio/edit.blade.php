@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Orden de Trabajo {{$trabajo->Id}}	</h3>
		<input type="hidden" id="ot_id" value="{{$trabajo->Id}}">
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
</div>

{!! Form::model($trabajo,['method'=>'PATCH','route'=>['servicio.update',$trabajo->Id]]) !!}

{{Form::token()}}

<div class="row">
	

	<div class="col-lg-5 col-md-5 col-md-5 col-xs-5">
		
		<div class="col-lg-6 col-md-6 col-md-6 col-xs-6">
			<div class="form-group">

				<label>Fecha de Entrada</label>
				<input type="date" name="fecha_entrada" id="fecha_entrada" class="form-control" value="{{$trabajo->FechaEntrada->format('Y-m-d')}}" disabled="">

				<label>Fecha de Salida: </label>
				@if($trabajo->FechaSalida!=null)
				<input type="date" name="fecha_salida" id="fecha_salida" class="form-control" value="{{$trabajo->FechaSalida->format('Y-m-d')}}">
				@else
				<input type="date" name="fecha_salida" id="fecha_salida" class="form-control" value="">
				@endif
				<label>Documento:  </label>
				<select class="form-control">
					<option>Boleta</option>
					<option>Recibo</option>
					<option>Factura</option>
				</select>
			</div>

		</div>

		<div class="col-lg-6 col-md-6 col-md-6 col-xs-6">
			<div class="form-group">

				<label>Hora de Entrada</label>
				<input type="time" name="hora_entrada" class="form-control" value="{{$trabajo->FechaEntrada->format('H:i')}}" disabled>
				<label>Hora de Salida: </label>
				<input type="time" name="hora_salida" id="hora_salida" class="form-control">
				<label>Estado:  </label>
				<select class="form-control" name="id_estado">
					@foreach($estados as $estado)
					@if($trabajo->Estado == $estado->Id)
					<option value="{{$estado->Id}}" selected>{{$estado->Descripcion}}</option>
					@else
					<option value="{{$estado->Id}}">{{$estado->Descripcion}}</option>
					@endif
					@endforeach
				</select>
			</div>
		</div>


		<div class="row">

			<div class="col-lg-12">

				<div class="form-group">
					<label for="cliente">Cliente</label>
				</div>

				<div class="col-lg-6">
					<div class="form-group">
						<input type="text" name="cliente" id="cliente" class="form-control" value="{{$cliente->TipoDocumento}} {{$cliente->Id}}" disabled>
					</div>
				</div>

				<div class="col-lg-6">
					<div class="form-group">
						<input type="text" name="cliente" id="cliente" class="form-control" value="Tel: {{$cliente->Telefono}}" disabled>
					</div>
				</div>

				<div class="col-lg-12">
					<div class="form-group">
						<input type="text" name="cliente" id="cliente" class="form-control" value="Nombres: {{$cliente->Nombres}}" disabled>
					</div>
				</div>
			</div>
		</div>

		

		<div class="form-group">
			<label for="cliente">Moto</label>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<input type="text" name="moto" id="moto" class="form-control" value="Placa: {{$moto->Placa}}" disabled>
			</div>
			<div class="form-group">
				<input type="text" name="moto" id="moto" class="form-control" value="Marca: {{$moto->Marca}}" disabled>
			</div>
			<div class="form-group">
				<input type="text" name="moto" id="moto" class="form-control" value="Color: {{$moto->Color}}" disabled>
			</div>
		</div>

		
	</div>
	

	<div class="col-lg-1 col-md-1 col-md-1 col-xs-1">
	</div>
	<div class="col-lg-5">
		<input id="hiddenMotoMarcas" name="hiddenMotoMarcas" type="hidden" value="101,50;120,30;90,20">

		
		<img id="myImgId" alt="" src="{{asset('img/moto.jpg')}}" width="400" height="auto" align="center" onMouseDown="GetCoordinates();" />
		<div id="planet.1" class="circulo"></div>
		<div id="emptyPlanet"></div>
		
		<p>X:<span id="x"></span></p>
		<p>Y:<span id="y"></span></p>

	</div>
	<div class="col-lg-1 col-md-1 col-md-1 col-xs-1">
	</div>
</div>



<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<p for="notes">Descripcion del Servicio</p>
			<textarea name="descripcion_servicio" class="form-control" rows="3">{{$trabajo->DescripcionServicio}}</textarea>
		</div>

	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<p for="notes">Trabajos Realizados</p>
			<textarea name="trabajos_realizados" class="form-control" rows="3" >{{$trabajo->TrabajosRealizados}}</textarea>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="form-group">
			<h2>Repuesto</h2>
		</div>

	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			
			<label class="sr-only" for="repuesto"></label>
			<input type="text" name="repuesto" id="repuesto" class="form-control" placeholder="Buscar Repuesto">		

		</div>
		

	</div>
	<div class="col-lg-1">
		<div class="form-group">

			<button type="reset" id="boton_reset_repuesto" class="btn btn-danger">X</button>	

		</div>

	</div>
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs">
		<div class="form-group">
			<button id="btn-agregar-repuesto" class="btn btn-primary">Agregar Repuestos / Insumos</button>


			
		</div>
	</div>
	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
		<a href="" data-target="#modal-repuesto-nuevo" data-toggle="modal"><button class="btn btn-danger">Nuevo Repuesto</button></a>
	</div>

</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table id="lista-repuestos" class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<tr>
						<th>Cantidad</th>
						<th>Descripcion</th>
						<th>Monto</th>
						<th></th>
					</tr>			
				</thead>
				<tbody>
					<div id="lista_contenido">
						@php ($i = 0) 
						@foreach ($repuestos as $repuesto)
						
						<tr id='row{{$i}}'> 
							<td>
								<input type='hidden' name='id[]' id='id_repuesto_{{$i}}' value='{{$repuesto->Id}}'>
								<input type="text" name="cantidad[]" id="cantidad_{{$i}}" value="{{$repuesto->Cantidad}}" class="form-control input_cantidad">
							</td>
							<td>
								<input type="text" name="" value="{{$repuesto->Descripcion}}" class="form-control" disabled="">
							</td>
							<td>
								<input type="text" name="precio_venta[]" id="precio_venta{{$i}}" value="{{$repuesto->Monto}}" class="form-control">
							</td>
							<td>
								<button id="{{$i}}" class="btn btn-danger btn_eliminar_repuesto">X</button>
							</td>
						</tr>
						@php ($i = $i + 1) 
						@endforeach
						<input type="hidden" id="contador" value="{{$i}}">
					</div>
					
				</tbody>

			</table>
		</div>

	</div>
</div>



<div class="row">
	<div class="col-lg-4 col-lg-offset-5 col-md-4 col-md-offset-5 col-sm-4 col-sm-offset-5 col-xs-12 col-xs-offset-8">
		
		<div class="form-group">
			<button class="btn btn-success" type="submit">Guardar</button>
		</div>
	</div>
</div>



{!! Form::close() !!}


@include('servicio.modal-repuesto-nuevo')

@endsection