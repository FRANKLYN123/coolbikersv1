<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-moto-nuevo">

	<div class="modal-dialog">

		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>

				<h2 class="text-center text-primary">Moto Nuevo</h2>
				<div id="campo_respuesta_moto">


				</div>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label for="descripcion">Placa</label>
							<input type="text" id="placa" name="descripcion"  class="form-control">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label for="descripcion">Modelo</label>
							<input type="text" id="modelo" name="descripcion" value="{{old('descripcion')}}" class="form-control">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<label for="descripcion">Marca</label>
							<input type="text" id="marca" name="descripcion" value="{{old('descripcion')}}" class="form-control">
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label for="descripcion">KM</label>
							<input type="number" id="km" name="descripcion" value="{{old('descripcion')}}" class="form-control">
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label for="descripcion">Numero de Motor</label>
							<input type="text" id="numero_motor" name="descripcion" value="{{old('descripcion')}}" class="form-control">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Color</label>
						</div>
						<div class="col-lg-1">

							<label class="container">
								<input type="radio" checked="checked" name="radio" value="rojo">
								<span class="checkmark" style="background-color: red;"></span>
							</label>

						</div>
						<div class="col-lg-1">

							<label class="container">
								<input type="radio" checked="checked" name="radio" value="naranja">
								<span class="checkmark" style="background-color: orange;"></span>
							</label>

						</div>
						<div class="col-lg-1">

							<label class="container">
								<input type="radio" checked="checked" name="radio" value="morado">
								<span class="checkmark" style="background-color: purple;"></span>
							</label>

						</div>
						
						<div class="col-lg-1">
							<label class="container">
								<input type="radio" name="radio" value="azul">
								<span class="checkmark" style="background-color: blue;"></span>
							</label>

						</div>

						<div class="col-lg-1">
							<label class="container">
								<input type="radio" name="radio" value="celeste">
								<span class="checkmark" style="background-color: skyblue;"></span>
							</label>

						</div>

						<div class="col-lg-1">
							<label class="container">
								<input type="radio" name="radio" value="verde">
								<span class="checkmark" style="background-color: green;"></span>
							</label>
						</div>

						<div class="col-lg-1">
							<label class="container">
								<input type="radio" name="radio" value="amarillo">
								<span class="checkmark" style="background-color: yellow;"></span>
							</label>
						</div>

						<div class="col-lg-1">
							<label class="container">
								<input type="radio" name="radio" value="gris">
								<span class="checkmark" style="background-color: gray;"></span>
							</label>
						</div>
						<div class="col-lg-1">
							<label class="container">
								<input type="radio" name="radio" value="negro">
								<span class="checkmark" style="background-color: black;"></span>
							</label>
						</div>
					</div>
					<div class="form-group">
						.
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label for="descripcion">Serie</label>
							<input type="text" id="numero_serie" name="descripcion" value="{{old('descripcion')}}" class="form-control">
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label for="descripcion">Año de Fabricacion</label>
							<input type="number" id="año_fabricacion" name="año_fabricacion"  class="form-control">
						</div>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" id="btn-guardar-moto" class="btn btn-primary">Guardar</button>
			</div>
		</div>

	</div>
</div>