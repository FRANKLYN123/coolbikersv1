@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Orden de Trabajo	</h3>
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
</div>

{!! Form::open(array('url'=>'servicio','method'=>'POST','autocomplete'=>'off')) !!}

{{Form::token()}}

<div class="row">
	

	<div class="col-lg-5 col-md-5 col-md-5 col-xs-5">
		
		<div class="col-lg-6 col-md-6 col-md-6 col-xs-6">
			<div class="form-group">

				<label>Fecha de Entrada</label>
				<input type="date" name="fecha_entrada" id="fecha_entrada" class="form-control" >


				<label>Fecha de Salida: </label>
				<input type="date" name="fecha_salida" id="fecha_salida" class="form-control">
				<label>Documento:  </label>
				<select class="form-control" id="documento" name="documento">
					<option value="boleta">Boleta</option>
					<option value="recibo">Recibo</option>
					<option value="Factura">Factura</option>
				</select>
			</div>

		</div>
		<div class="col-lg-6 col-md-6 col-md-6 col-xs-6">
			<div class="form-group">

				<label>Hora de Entrada</label>
				<input type="time" name="hora_entrada" id="hora_entrada" class="form-control" >
				<label>Hora de Salida: </label>
				<input type="time" name="hora_salida" id="hora_salida" class="form-control">
				<label>Estado:  </label>
				<select class="form-control" name="id_estado">
					@foreach($estados as $estado)
					<option value="{{$estado->Id}}">{{$estado->Descripcion}}</option>
					
					@endforeach
				</select>
			</div>
		</div>



		<div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
			<div class="form-group">

				<label for="cliente">Cliente</label>
				<input type="text" name="cliente" id="cliente" class="form-control" placeholder="Buscar Cliente" >

			</div>

			<div class="form-group">

				<a href="" data-target="#modal-cliente-nuevo" data-toggle="modal"><button class="btn btn-primary">Nuevo Cliente</button></a>


			</div>




			<div class="form-group">

				<label for="moto">Moto</label>
				<input type="text" name="moto" id="moto" class="form-control" placeholder="Buscar Moto" >

			</div>

			<div class="form-group">

				<a href="" data-target="#modal-moto-nuevo" data-toggle="modal"><button class="btn btn-primary">Nuevo Moto</button></a>

			</div>


		</div>
	</div>
	<div class="col-lg-1 col-md-1 col-md-1 col-xs-1">
	</div>
	<div class="col-lg-5 col-md-5 col-md-5 col-xs-5">
		<input id="hiddenMotoMarcas" name="hiddenMotoMarcas" type="hidden" value="101,50;120,30;90,20">

		
		<img id="myImgId" alt="" src="{{asset('img/moto.jpg')}}" width="400" height="auto" align="center" onMouseDown="GetCoordinates();" />
		<div id="planet.1" class="circulo"></div>
		<div id="emptyPlanet"></div>
		
		<p>X:<span id="x"></span></p>
		<p>Y:<span id="y"></span></p>

	</div>
	<div class="col-lg-1 col-md-1 col-md-1 col-xs-1">
	</div>
</div>



<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<p for="notes">Descripcion del Servicio</p>
			<textarea name="descripcion_servicio" class="form-control" rows="3" ></textarea>
		</div>

	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<p for="notes">Trabajos Realizados</p>
			<textarea name="trabajos_realizados" class="form-control" rows="3" ></textarea>
		</div>

	</div>
</div>

<div class="row" >
	<div class="col-lg-2">
		<div id="mensaje_error_guardar_orden" class="alert-danger"></div>
	</div>
</div>




<div class="row">
	<div class="col-lg-4 col-lg-offset-5 col-md-4 col-md-offset-5 col-sm-4 col-sm-offset-5 col-xs-12 col-xs-offset-8">
		
		<div class="form-group">
			<button class="btn btn-success" type="submit">Guardar</button>
		</div>
	</div>
</div>

{!! Form::close() !!}

@include('servicio.modal-cliente-nuevo')
@include('servicio.modal-moto-nuevo')
@include('servicio.modal-repuesto-nuevo')

@endsection