@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-8">

		<h1>
			Orden de Trabajo      
			<a href="servicio/create">
				<button class="btn btn-success">Nuevo</button>
			</a>
		</h1>
		
	</div>
	<div class="col-lg-4">
		@include('servicio.search')
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table">
				
				<thead class="thead-dark">
					<tr>
						<th scope="col">OT</th>
						<th scope="col">Cliente</th>
						<th scope="col">Modelo</th>
						<th scope="col">Marca</th>
						<th scope="col">Placa</th>
						<th scope="col">Descripcion</th>
						<th scope="col">Fecha</th>
						<th scope="col">Estado</th>
						<th scope="col">Recibo</th>



					</tr>
				</thead>
				
				@foreach ($trabajos as $trabajo)
				
				@if($trabajo->IdEstado == 1)
				<tr style="background-color: #8CDCDA">
					@elseif($trabajo->IdEstado == 2)
					<tr style="background-color: #85929E">
						@elseif($trabajo->IdEstado == 3)
						<tr style="background-color: #F1948A">

							@else
							<tr style="background-color: #B1D877">

								@endif



								<td><a href="{{URL::action('OrdenTrabajoController@edit',$trabajo->Id)}}" class="indice_tabla">{{$trabajo->Id}}</a></td>
								<td >{{$trabajo->Cliente}}</td>
								<td>{{$trabajo->Modelo}}</td>
								<td>{{$trabajo->Marca}}</td>
								<td>{{$trabajo->Placa}}</td>
								<td>{{$trabajo->DescripcionServicio}}</td>
								<td>{{$trabajo->FechaEntrada}}</td>
								<td>{{$trabajo->Estado}}</td>

								<td>
									<a href="{{URL::action('OrdenTrabajoController@show', $trabajo->Id)}}"><button class="btn btn-danger">A4</button></a>
									<a href="{{URL::action('TicketController@index', $trabajo->Id)}}"><button class="btn btn-danger">TCK</button></a>
								</td>
							</tr>


							@endforeach
						</table>
					</div>
					{{$trabajos->render()}}
				</div>
			</div>

			@endsection