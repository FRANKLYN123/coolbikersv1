@extends ('layouts.admin')
@section ('contenido')

	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Ingresar Repuestos	</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>
	
	{!! Form::open(array('url'=>'almacen/repuesto','method'=>'POST','autocomplete'=>'off')) !!}

	{{Form::token()}}

	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="descripcion">Descripcion</label>
				<input type="text" name="descripcion" value="{{old('descripcion')}}" class="form-control" placeholder="Descripcion" required>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="categoria">Categoria</label>
				<select class="form-control" name="idcategoria">

					@foreach ($categorias as $categoria)
						<option value="{{$categoria->Id}}">{{$categoria->Descripcion}}</option>
					@endforeach
					
					
				</select>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="idum">Unidad de Medida</label>
				<select class="form-control" name="idum">
					@foreach ($unidades_medida as $unidad_medida)
						<option value="{{$unidad_medida->Id}}">{{$unidad_medida->Descripcion}}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="cantidad">Cantidad</label>
				<input type="number" name="cantidad" value="0" class="form-control" placeholder="Cantidad" required="">
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="idproveedor">Proveedor</label>
				<select class="form-control" name="idproveedor">
					@foreach ($proveedores as $proveedor)
						<option value="{{$proveedor->Ruc}}">{{$proveedor->RazonSocial}}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="precioventa">Precio de venta</label>
				<input type="number" name="precio_venta" step="0.01" value="0.00" class="form-control" placeholder="Precio de Venta">
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>

			</div>

		</div>
			

	</div>
	{!! Form::close() !!}

@endsection