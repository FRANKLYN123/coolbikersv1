@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<a href="repuesto/create"
			><button class="btn btn-success">Ingresar Repuestos</button></a>
		<h3>
			Repuestos
		</h3>
		@include('almacen.repuesto.search')
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Id</th>
					<th>Descripcion</th>
					<th>IdCategoria</th>
					<th>UDM</th>
					<th>Cantidad</th>
					<th>Precio</th>
					<th>Opciones</th>
					
				</thead>
				@foreach ($repuestos as $repuesto)
				<tr>
					<td>{{ $repuesto->Id }}</td>
					<td>{{ $repuesto->Descripcion }}</td>
					<td>{{ $repuesto->Categoria }}</td>
					<td>{{ $repuesto->UnidadMedida }}</td>
					<td>{{ $repuesto->Cantidad }}</td>
					<td>S/ {{ number_format($repuesto->PrecioVenta,2) }}</td>
					<td>

						<a href="" data-target="#modal-mas-{{$repuesto->Id}}" data-toggle="modal"><button class="btn btn-success">Ver mas</button></a>
						<a href="{{URL::action('RepuestoController@edit',$repuesto->Id)}}"><button class="btn btn-primary">Editar</button></a>
						
						
					</td>


				</tr>
				@include('almacen.repuesto.modal')
				@endforeach
			</table>
		</div>
		{{$repuestos->render()}}
	</div>
</div>

@endsection