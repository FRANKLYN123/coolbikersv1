@extends ('layouts.admin')
@section ('contenido')

	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Editar Repuesto {{$repuesto->Descripcion}} </h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>

			

	{!! Form::model($repuesto,['method'=>'PATCH','route'=>['almacen.repuesto.update',$repuesto->Id]]) !!}

	{{Form::token()}}

	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="descripcion">Descripcion</label>
				<input type="text" name="descripcion" value="{{$repuesto->Descripcion}}" class="form-control" required>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="categoria">Categoria</label>
				<select class="form-control" name="idcategoria">

					@foreach ($categorias as $categoria)
						@if ($categoria->Id == $repuesto->IdCategoria)
							<option value="{{$categoria->Id}}" selected>{{$categoria->Descripcion}}</option>
						@else
							<option value="{{$categoria->Id}}" >{{$categoria->Descripcion}}</option>
						@endif
					@endforeach
					
					
				</select>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="idum">Unidad de Medida</label>
				<select class="form-control" name="idum">
					@foreach ($unidades_medida as $unidad_medida)
						@if ($unidad_medida->Id == $repuesto->IdUnidadMedida)
							<option value="{{$unidad_medida->Id}}" selected>{{$unidad_medida->Descripcion}}</option>
						@else
							<option value="{{$unidad_medida->Id}}">{{$unidad_medida->Descripcion}}</option>
						@endif
					@endforeach
				</select>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="cantidad">Cantidad</label>
				<input type="text" name="cantidad" value="{{$repuesto->Cantidad}}" class="form-control" required>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="idproveedor">Proveedor</label>
				<select class="form-control" name="idproveedor">
					@foreach ($proveedores as $proveedor)
						<option value="{{$proveedor->Ruc}}">{{$proveedor->RazonSocial}}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="preciocompra">Precio de compra</label>
				<input type="text" name="precio_compra" value="" class="form-control" required>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="precioventa">Precio de venta</label>
				<input type="text" name="precio_venta" value="{{$repuesto->PrecioVenta}}" class="form-control">
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label for="fecha">fecha</label>
				<input type="date" name="fecha" class="form-control">
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>

			</div>

		</div>
			

	</div>
	{!! Form::close() !!}

		

@endsection