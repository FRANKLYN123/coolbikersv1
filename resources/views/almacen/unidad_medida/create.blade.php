@extends ('layouts.admin')
@section ('contenido')

	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Nueva Unidad de Medida</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
			@endif

			{!! Form::open(array('url'=>'almacen/unidad_medida','method'=>'POST','autocomplete'=>'off')) !!}

			{{Form::token()}}

			<div class="form-group">
				<label for="descripcion">Descripcion</label>
				<input type="text" name="descripcion" class="form-control" placeholder="Descripcion" value="{{old('descripcion')}}" required>
			</div>

			<div class="form-group">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<a href="{{url('almacen/unidad_medida')}}"><button class="btn btn-danger">Cancelar</button></a>

			</div>
			{!! Form::close() !!}

		</div>
	</div>

@endsection