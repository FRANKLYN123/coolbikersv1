@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h3>
			Categorias      <a href="unidad_medida/create"
			><button class="btn btn-success">Nuevo</button></a>
		</h3>
		@include('almacen.unidad_medida.search')
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Id</th>
					<th>Descripcion</th>
					<th>Opciones</th>
					
				</thead>
				@foreach ($unidades_medida as $unidad_medida)
				<tr>
					<td>{{ $unidad_medida->Id }}</td>
					<td>{{ $unidad_medida->Descripcion }}</td>
					<td>
						<a href="{{URL::action('UnidadMedidaController@edit',$unidad_medida->Id)}}"><button class="btn btn-primary">Editar</button></a>
						<a href="" data-target="#modal-delete-{{$unidad_medida->Id}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
						
						
					</td>


				</tr>
				@include('almacen.unidad_medida.modal')
				@endforeach
			</table>
		</div>
		{{$unidades_medida->render()}}
	</div>
</div>

@endsection