@extends ('layouts.admin')
@section ('contenido')

  <br>
  <br>

  <div class="container text-center">
    <div class="jumbotron">
      <h1>Crear una aplicación crud sin recargar la pagina en laravel</h1>
    </div>
  </div>

  <div class="container">
    <div class="panel panel-primary">
      <div class="panel-heading">Crear una aplicación crud sin recargar la pagina en laravel

      </div>
      <div class="panel-body">
        <button id="btn_add" name="btn_add" class="btn btn-primary pull-right">Nuevo Producto</button>
        <table class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Descripcion</th>
              <th>Accion</th>
            </tr>
          </thead>
          <tbody id="productos-list" name="productos-list">
            @foreach ($productos as $producto)
            <tr id="producto{{$producto->id}}">
              <td>{{$producto->id}}</td>
              <td>{{$producto->nombre}}</td>
              <td>{{$producto->descripcion}}</td>
              <td>
                <button class="btn btn-warning btn-detail open_modal" value="{{$producto->id}}">Editar</button>
                <button class="btn btn-danger btn-delete delete-producto" value="{{$producto->id}}">Eliminar</button>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    @include('producto.modal')
  </div>
  <meta name="_token" content="{!! csrf_token() !!}" />

@endsection