<link href="css/bootstrap.min.css" rel="stylesheet">	

<style type="text/css">

.col-centered{
	float: none;
	margin: 0 auto;
}
.panel-height {
	height: 200px;
}

.panel-custom {
	padding: 2px 4px 3px 10px;
}

.titulo_inicio {
	padding-right: 0px;
}

.titulo {
	padding-left: 0px;
	padding-right: 0px;
}

.contenido {
	padding-left: -15px;
}

.contenido_serie, .contenido_motor {
	padding-left: -15px;
}

.titulo_modelo {
	padding-left: 30px;
}

.telefono {
	padding-left: 15px;
}

.contenido_modelo {
	padding-left: -70px;
}


</style>

<div class="row">
	<div class="col-xs-3">
		<img src="img/logo_empresa.jpg" height="auto" width="200px" class="img-rounded" alt="Cinque Terre">
	</div>
	<div class="col-xs-8">
		<h1>Orden de Trabajo N º {{ $trabajo['Id'] }}</h1>	

		<strong>Hrs E: </strong>
		<span class="contenido">{{ $trabajo['FechaEntrada']->format("H:i")}}</span>
		<strong>Hrs S:</strong>
		<span></span>
		<strong>Fecha:</strong>
		<span>{{ $trabajo['FechaEntrada']->format("d-m-Y")}}</span>

	</div>
	
	
</div>


<div class="panel panel-primary">
	<div class="panel-body panel-custom">

		<div class="row">
			<div class="col-xs-1">
				<strong class="m-0">Señor(es): </strong>

			</div>
			<div class="col-xs-11">
				<p>{{ $cliente['Nombres']}}</p>

			</div>

		</div>

		<div class="row">
			<div class="col-xs-1">
				<strong>Direccion: </strong>
			</div>
			<div class="col-xs-6">
				<p>{{ $cliente['Direccion']}}</p>
			</div>

			<div class="col-xs-1 contenido">
				<strong>D.N.I. :</strong>
			</div>

			<div class="col-xs-1 contenido">
				<p>{{ $cliente['Id']}}</p>
			</div>


		</div>

		<div class="row">
			<div class="col-xs-1">
				<strong>Correo: </strong>
			</div>
			<div class="col-xs-2">
				<p>{{ $cliente['Correo']}}</p>
			</div>

			<div class="col-xs-1 titulo">
				<strong>Telefono: </strong>
			</div>
			<div class="col-xs-2 telefono">
				<p>{{ $cliente['Telefono']}}</p>
			</div>


			<div class="col-xs-2 titulo_t_propiedad">
				<strong>T. propiedad:</strong>
			</div>
			<div class="col-xs-1 contenido">
				<p></p>
			</div>
		</div>

	</div>
</div>


<div class="panel panel-primary">
	<div class="panel-body panel-custom">

		<div class="row">

			<div class="col-xs-1 titulo_inicio">
				<strong>Placa:</strong> 
			</div>
			<div class="col-xs-1 contenido">
				<p>{{ $trabajo['Placa']}}</p> 
			</div>

			<div class="col-xs-1 titulo">
				<strong>Color:</strong>
			</div>
			<div class="col-xs-1 contenido">
				<p>{{ $moto['Color']}}</p>
			</div>

			<div class="col-xs-1 titulo">
				<strong>Marca:</strong> 
			</div>
			<div class="col-xs-1 contenido">
				<p>{{ $moto['Marca']}}</p>
			</div>

			<div class="col-xs-2 titulo_modelo">
				<strong>Modelo: </strong>
			</div>
			<div class="col-xs-1 contenido_modelo">
				<p>{{ $moto['Modelo']}}</p>
			</div>

			<div class="col-xs-1 titulo">
				<strong>KM:</strong> 
			</div>
			<div class="col-xs-1 contenido">
				<p>{{ $moto['Km']}}</p> 
			</div>

		</div>

		<div class="row">

			

			<div class="col-xs-1 titulo_inicio">
				<strong>Serie:</strong>
			</div>
			<div class="col-xs-3 contenido_serie">
				<p>{{ $moto['NumeroSerie']}}</p>
			</div>

			<div class="col-xs-1 titulo">
				<strong>Motor:</strong>
			</div>
			<div class="col-xs-2 contenido_motor">
				<p>{{ $moto['NumeroMotor']}}</p>
			</div>

			<div class="col-xs-1 titulo">
				<strong>Casco:</strong>
			</div>
			<div class="col-xs-1 contenido">
				<p></p>
			</div>

			<div class="col-xs-1 titulo_guantes">
				<strong>Guantes:</strong>
			</div>
			<div class="col-xs-1 contenido">
				<p></p>
			</div>

		</div>

	</div>
</div>

<div class="row">
	<span>.</span>
</div>


<div class="row">
	<div class="col-xs-5">
		<div class="panel panel-primary">
			<div class="panel-heading">Descripcion</div>
			<div class="panel-body panel-height">

				{{ $trabajo['DescripcionServicio']}}

			</div>
		</div>
	</div>
	<div class="col-xs-7">
		<span></span>
		<img src="img/moto.jpg" height="320px" width="auto" class="img-rounded" alt="Cinque Terre">
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Trabajos Realizados</div>
			<div class="panel-body">
				..................................................................................................................................................................................
				..................................................................................................................................................................................
				..................................................................................................................................................................................
				..................................................................................................................................................................................
				..................................................................................................................................................................................

			</div>
		</div>
	</div>
</div>

<div class="row">
</div>
<div class="row">
	
	<div class="col-xs-12 text-center">

		<span>___________________</span>                    
		<p>FIRMA DEL ASESOR</p>						

	</div>
	
</div>