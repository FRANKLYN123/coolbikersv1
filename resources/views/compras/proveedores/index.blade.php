@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h3>
			Proveedores      <a href="proveedores/create"
			><button class="btn btn-success">Nuevo</button></a>
		</h3>
		@include('compras.proveedores.search')
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Ruc</th>
					<th>Razon Social</th>
					<th>Direccion</th>
					<th>Sucursal</th>
					<th>Telefono</th>
					<th>Correo</th>
					<th>Pagina Web</th>
					<th>Opciones</th>
					
				</thead>
				@foreach ($proveedores as $proveedor)
				<tr>
					<td>{{ $proveedor->Ruc }}</td>
					<td>{{ $proveedor->RazonSocial }}</td>
					<td>{{ $proveedor->DireccionFiscal }}</td>
					<td>{{ $proveedor->Sucursal }}</td>
					<td>{{ $proveedor->Telefono }}</td>
					<td>{{ $proveedor->Correo }}</td>
					<td>{{ $proveedor->PaginaWeb }}</td>
					<td>
						<a href="{{URL::action('ProveedorController@edit',$proveedor->Ruc)}}"><button class="btn btn-primary">Editar</button></a>
						<a href="" data-target="#modal-delete-{{$proveedor->Ruc}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
					</td>
				</tr>
				
				@endforeach
			</table>
		</div>
		{{$proveedores->render()}}
	</div>
</div>

@endsection