@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Nuevo Proveedor</h3>

		{!! Form::open(array('url'=>'compras/proveedores','method'=>'POST','autocomplete'=>'off')) !!}

		{{Form::token()}}

		<div class="form-group">
			<label for="ruc">R.U.C.</label>
			<input type="number" name="ruc" class="form-control" placeholder="RUC">
		</div>
		<div class="form-group">
			<label for="razon_social">Razon Social</label>
			<input type="text" name="razon_social" class="form-control" placeholder="Razon Social">
		</div>

		<div class="form-group">
			<label for="direccion">Direccion Fiscal</label>
			<input type="text" name="direccion" class="form-control" placeholder="Direccion Fiscal">
		</div>

		<div class="form-group">
			<label for="sucursal">Sucursal</label>
			<input type="text" name="sucursal" class="form-control" placeholder="Sucursal">
		</div>
		<div class="form-group">
			<label for="telefono">Telefono</label>
			<input type="number" name="telefono" class="form-control" placeholder="Telefono">
		</div>
		<div class="form-group">
			<label for="correo">Correo</label>
			<input type="mail" name="correo" class="form-control" placeholder="Correo">
		</div>
		<div class="form-group">
			<label for="pagina_web">Pagina Web</label>
			<input type="text" name="pagina_web" class="form-control" placeholder="Pagina Web">
		</div>

		<div class="form-group">
			<button class="btn btn-primary" type="submit">Guardar</button>
			<button class="btn btn-danger" type="reset">Cancelar</button>

		</div>
		{!! Form::close() !!}

	</div>
</div>

@endsection