<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  

  <title>CoolBikers</title>

  
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('css/_all-skins.min.css')}}">

  <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">
  <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
  
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

  <!--  jQuery -->
  <script src="{{asset('plugins/jQuery/jquery-3.3.1.min.js')}}"></script>
  

  <link rel="stylesheet" href="{{asset('css/custom.css')}}">
  <style type="text/css">


</style>
</head>
<body class="hold-transition skin-blue sidebar-mini" >
  <div class="wrapper">

    <header class="main-header">

      <!-- Logo -->
      <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>CB</b>V</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>CoolBikers</b></span>
      </a>

      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Navegación</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">


              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">


                </li>

                <!-- Menu Footer-->
                <li class="user-footer">

                  <div class="pull-right">
                    <a href="#" class="btn btn-default btn-flat">Cerrar</a>
                  </div>
                </li>
              </ul>
            </li>

          </ul>
        </div>

      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header"></li>

          <li class="treeview">
            <a href="">
              <i class="fa fa-laptop"></i>
              <span>Inventario</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>

            <ul class="treeview-menu">
              <li><a href="{{url('inventario')}}"><i class="fa fa-circle-o"></i>Inventario</a></li>
              <li><a href="{{url('almacen/categoria')}}"><i class="fa fa-circle-o"></i> Categorías</a></li>
              <li><a href="{{url('almacen/unidad_medida')}}"><i class="fa fa-circle-o"></i> Unidades de Medida</a></li>
            </ul>
          </li>

          

          <li class="treeview">
            <a href="#">
              <i class="fa fa-th"></i>
              <span>Compras</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('compras/proveedores')}}"><i class="fa fa-circle-o"></i> Proveedores</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="{{url('servicio')}}">
              <i class="fa fa-shopping-cart"></i>
              <span>Orden de Trabajo</span>
            </a>
          </li>
          <li class="treeview">
            <a href="{{url('cliente')}}">
              <i class="fa fa-users"></i>
              <span>Clientes</span>
            </a>
          </li>
          <li class="treeview">
            <a href="{{url('caja')}}">
              <i class="fa fa-laptop"></i>
              <span>Caja</span>
            </a>
          </li>

          

        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!--Contenido-->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-md-12">
            <div class="box">

              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <!--Contenido-->

                    @yield('contenido')

                    <!--Fin Contenido-->
                  </div>
                </div>

              </div>
            </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->


      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <!--Fin-Contenido-->
    <footer class="main-footer">


      <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.0
      </div>
      <strong>Copyright &copy; 2015-2020  All rights reserved.
      </footer>

      
      

      <link rel="stylesheet" type="text/css" href="{{asset('css/mapping.css')}}"> 
      <script src="{{asset('js/mapping.js')}}" ></script>
      <script src="{{asset('https://code.jquery.com/jquery-3.1.0.js')}}"></script>

      <!--AUTOCOMPLETE-->
      <link rel="stylesheet" href="{{asset('plugins/jQuery/jquery-ui.css')}}">
      <script src="{{asset('plugins/jQuery/jquery-ui.js')}}"></script>
      
      
      <script src="{{asset('js/app.min.js')}}"></script>
      <script src="{{asset('js/custom.js')}}"></script>
      <script src="{{asset('js/bootstrap.min.js')}}"></script>

    </body>
    </html>
