<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class UnidadMedida extends Model
{
	protected $table = 'UnidadMedida';

	protected $primaryKey = 'Id';

	public $timestamps = false;

	protected $fillable = [
		'Descripcion',
	];


	protected $guarded = [

	];
}
