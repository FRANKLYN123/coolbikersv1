<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{
    protected $table = 'Movimiento';

    protected $primaryKey = 'Id';

    public $timestamps = false;

    protected $fillable = [

    	'IdRepuesto',
    	'Fecha',
    	'Cantidad',
    	'TipoMovimiento',
    	'Stock',
    	'SubtipoMovimiento',
    	'IdSubtipoMovimiento'

    ]; 

    protected $guarded = [

    ];
}
