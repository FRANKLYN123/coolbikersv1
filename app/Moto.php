<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class Moto extends Model
{
	protected $table = 'Moto';

	protected $primaryKey = 'Placa';

	public $timestamps = false;

	protected $fillable = [
		'Placa',
		'Marca',
		'Color',
		'Modelo',
		'NumeroMotor',
		'NumeroSerie',
		'AñoFabricacion',
		'Km',

	]; 

	protected $guarded = [

	];
}
