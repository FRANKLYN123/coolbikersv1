<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class OrdenTrabajoDetalle extends Model
{
    protected $table = 'OrdenTrabajoDetalle';

    protected $primaryKey = 'Id';

    public $timestamps = false;

    protected $fillable = [
    	'IdOrdenTrabajo',
    	'CodigoRepuesto',
    	'Cantidad',
    	'Monto'
    ];

    protected $guarded = [

    ];
}
