<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class Caja extends Model
{
	protected $table = 'Caja';

	protected $primaryKey = 'Id';

	public $timestamps = false;

	protected $fillable = [
		'descripcion',
		'monto',
		'saldo',
		'movimiento',
		'fechamovimiento'
	];


	protected $guarded = [

	];
}
