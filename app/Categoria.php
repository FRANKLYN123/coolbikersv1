<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    
    protected $table = 'Categoria';

    protected $primaryKey = 'Id';

    public $timestamps = false;

    protected $fillable = [
    	'Descripcion',
    	'Estado'
    ];


    protected $guarded = [

    ];
    
}
