<?php

namespace Sistema\Http\Requests;

use Sistema\Http\Requests\Request;

class RepuestoFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'descripcion' => 'required | max:100',
            'idcategoria' => 'max:5',
            'idum' => 'max:5',
            'cantidad' => 'required',
            'idproveedor' => 'required | max:11',
            'precio_compra' => 'required',
            'precio_venta' => 'required',
            'fecha' => 'required',
            
        ];
    }
}
