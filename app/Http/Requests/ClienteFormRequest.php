<?php

namespace Sistema\Http\Requests;

use Sistema\Http\Requests\Request;

class ClienteFormRequest extends Request
{
    
    public function authorize()
    {
        return true;
    }

    public function rules(){
        return [
            'numero_documento' => 'required|digits_between:8,11|unique:cliente,Id',
            'cliente' => 'required',
            'direccion' => 'required',
            'correo' => 'email',
            'telefono' => 'digits:9',
            
        ];
    }

    public function messages(){

        return [
            'numero_documento.digits_between' => 'El Numero de Documento debe ser de 8 u 11 digitos',
            'numero_documento.unique' => 'El cliente ya existe',
            'correo.email' => 'El correo no es valido'
        ];

    }
}
