<?php

namespace Sistema\Http\Requests;

use Sistema\Http\Requests\Request;

use DB;

class ClienteUpdateFormRequest extends Request
{
    
    public function authorize()
    {
        return true;
    }

    public function rules(){
        return [
            'cliente' => 'required',
            'direccion' => 'required',
            'correo' => 'email',
            'telefono' => 'digits:9',
            
        ];
    }

    public function messages(){

        return [
            
            'correo.email' => 'El correo no es valido'
        ];

    }
}