<?php

namespace Sistema\Http\Requests;

use Sistema\Http\Requests\Request;

class MotoFormRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules(){
        
        return [
            'Placa' => 'required|unique:moto',            
        ];
    }

    public function messages(){


        return [
            'Placa.unique' => 'La moto ya existe',
        ];


    }
}
