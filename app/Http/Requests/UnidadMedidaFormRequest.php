<?php

namespace Sistema\Http\Requests;

use Sistema\Http\Requests\Request;

class UnidadMedidaFormRequest extends Request
{
   
    public function authorize()
    {
        return true;
    }

    public function rules(){
        return [
            'descripcion' => 'required|unique:unidadmedida',
        ];
    }

    public function messages(){

        return [
            'descripcion.unique' => 'La Unidad de Medida ya existe',
        ];

    }
}
    
