<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Http\Request;




Route::get('/', function () {
	return Redirect::to('servicio');
});

Route::get('/test', function(){

	return view('welcome');    
});




Route::resource('almacen/categoria','CategoriaController');
Route::resource('almacen/repuesto','RepuestoController');
Route::resource('almacen/unidad_medida','UnidadMedidaController');
Route::resource('compras/proveedores','ProveedorController');
Route::resource('servicio','OrdenTrabajoController');
Route::resource('cliente','ClienteController');
Route::resource('moto','MotoController');
Route::resource('caja','CajaController');

//INVENTARIO
Route::resource('inventario','InventarioController');
Route::get('inventario_entrada','InventarioController@entrada');
Route::get('inventario_salida_venta_directa','InventarioController@salida_venta_directa');


Route::resource('ticket','TicketController');
Route::get('ticket/{id}','TicketController@index');

//REPUESTO
Route::get('repuesto','RepuestoController@modal_store');
Route::get('repuesto_ajax','OrdenTrabajoController@ajax_store');

Route::get('lista_repuestos/{id}','OrdenTrabajoController@lista_repuestos_ajax');

Route::get('eliminar_orden_detalle_ajax','OrdenTrabajoController@ajax_remove_orden_trabajo_detalle');
Route::get('actualizar_cantidad_orden_detalle_ajax','OrdenTrabajoController@ajax_actualizar_cantidad_orden_trabajo_detalle');

Route::get('searchCliente','ClienteController@search');
Route::get('searchMoto','MotoController@search');
Route::get('searchRepuesto','InventarioController@search');



Route::get('login', 'Auth\AuthController@showLoginForm');

Route::post('login', 'Auth\AuthController@login');
Route::get('logout', 'Auth\AuthController@logout');

        // Registration Routes...
Route::get('register', 'Auth\AuthController@showRegistrationForm');
Route::post('register', 'Auth\AuthController@register');

        // Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');

Route::get('/home', 'HomeController@index');

Route::auth();
