<?php

namespace Sistema\Http\Controllers;

use Illuminate\Http\Request;

use Sistema\Http\Requests;

use Sistema\Proveedor;

use Illuminate\Support\Facades\Redirect;

use DB;

class ProveedorController extends Controller
{
    public function __construct(){


    }

    public function index(Request $request){

    	if($request){
    		$query = trim($request->get('searchText'));
    		$proveedores = DB::table('Proveedor')->where('Ruc','LIKE','%'.$query.'%')->orWhere('RazonSocial','LIKE','%'.$query.'%')->where('Estado','=','1')->paginate(10);
    		return view('compras.proveedores.index',["proveedores"=>$proveedores,"searchText"=>$query]);
    	}
    }

    public function create(){

        
    	return view("compras.proveedores.create");

    }

    public function store(Request $request){

    	$proveedor = new Proveedor;
    	$proveedor->Ruc = $request->get('ruc');
    	$proveedor->RazonSocial = $request->get('razon_social');
    	$proveedor->DireccionFiscal = $request->get('direccion');
    	$proveedor->Sucursal = $request->get('sucursal');
    	$proveedor->Telefono = $request->get('telefono');
    	$proveedor->Correo = $request->get('correo');
    	$proveedor->PaginaWeb = $request->get('pagina_web');
    	    	
    	$proveedor->Estado = '1';
    	$proveedor->save();
    	return Redirect::to('compras/proveedores');

    }

    public function show($id){

    	return view("compras.proveedores.show",["proveedor"=>Proveedor::findOrFail($id)]);

    }

	public function edit($id){

		return view("compras.proveedores.edit",["proveedor"=>Proveedor::findOrFail($id)]);
    }

	public function update(Request $request,$id){

		$proveedor = Proveedor::findOrFail($id);
		$proveedor->Ruc = $request->get('ruc');
		$proveedor->update();
		return Redirect::to('compras/proveedores');

    }

	public function destroy($id){

		$proveedor = Proveedor::findOrFail($id);
		$proveedor->Estado = '0';
		$proveedor->update();
		return Redirect::to('compras/proveedores');

    }
}
