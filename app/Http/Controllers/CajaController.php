<?php

namespace Sistema\Http\Controllers;

use Illuminate\Http\Request;

use Sistema\Http\Requests;

use Sistema\Caja;
use Carbon\Carbon;

use Illuminate\Support\Facades\Redirect;

use Sistema\Http\Requests\CategoriaFormRequest;

use DB;


class CajaController extends Controller
{
    
    public function __construct(){


    }

    public function index(Request $request){

    	if($request){
    		$query = trim($request->get('searchText'));
    		$movimientos = DB::table('Caja')->orderBy('FechaMovimiento','desc')->paginate(10);
    		return view('caja.index')->with("movimientos",$movimientos)->with("searchText",$query);
    	}
    }


    public function store(Request $request){

        $saldo_anterior = Caja::all()->last()->Saldo;
                
        $caja = new Caja;
        $caja->descripcion = $request->input("descripcion");
        $monto = $request->input("monto");
        $caja->monto = $monto;
        $movimiento = $request->input("movimiento");

        if($movimiento=="entrada")
            $saldo = $saldo_anterior+$monto;
        else if($movimiento=="salida")
            $saldo = $saldo_anterior-$monto;
        
        $caja->movimiento = $movimiento;
        $caja->saldo = $saldo;
        $caja->fechamovimiento = Carbon::createFromFormat('Y-m-d',$request->input("fecha_movimiento"));
        $caja->save();
        
        return "";

    }

    public function show($id){


    }

	public function edit($id){

    }

	public function update(Request $request,$id){

    }

	public function destroy($id){

    }



}
