<?php

namespace Sistema\Http\Controllers;

use Illuminate\Http\Request;

use Sistema\Http\Requests;

use Sistema\Repuesto;
use Sistema\Movimiento;
use Sistema\Inventario;
use Sistema\Proveedor;
use Sistema\Categoria;
use Sistema\EntradaRepuesto;
use Sistema\SalidaVentaRepuesto;
use Sistema\Cliente;
use Sistema\Caja;
use Sistema\UnidadMedida;
use Carbon\Carbon;


use Illuminate\Support\Facades\Redirect;

use Sistema\Http\Requests\RepuestoFormRequest;



use DB;

class InventarioController extends Controller
{
     public function __construct(){

    }

    public function index(Request $request){

        if($request){
            $query = trim($request->get('searchText'));
            $repuestos = DB::table('Inventario as i')->join('Categoria as c','i.IdCategoria','=','c.Id')->join('UnidadMedida as u','i.IdUM','=','u.Id')->select('i.Id','i.Descripcion','c.Descripcion as Categoria','u.Descripcion as UM','i.PrecioVenta','i.Stock')->where('i.Estado','=','1')->paginate(10);

            $lista_repuestos = Inventario::all();
            $lista_proveedores = Proveedor::all();
            $lista_clientes = Cliente::all();
            $categorias = Categoria::all();
            $unidades_medida = UnidadMedida::all();

            return view('inventario.index')->with("repuestos",$repuestos)->with("searchText",$query)->with("lista_repuestos",$lista_repuestos)->with("lista_proveedores",$lista_proveedores)->with("lista_clientes",$lista_clientes)->with("categorias",$categorias)->with("unidades_medida",$unidades_medida);
        }
    }

    public function entrada(Request $request){


    	$id_repuesto = $request->input('Repuesto');
    	$cantidad = $request->input('Cantidad');
    	$fecha = Carbon::now();
    	$monto = $request->input('PrecioUnitario')*$cantidad;

    	$entrada = new EntradaRepuesto;
        $entrada->RucProveedor = $request->input('Proveedor');
        $entrada->IdRepuesto = $id_repuesto;
        $entrada->PrecioUnitario = $request->input('PrecioUnitario');
        $entrada->save();

        $inventario = Inventario::findOrFail($id_repuesto);
        $inventario->Stock = $inventario->Stock + $cantidad;
        $inventario->update();

        $movimiento = new Movimiento;
        $movimiento->IdRepuesto = $entrada->IdRepuesto;
        $movimiento->Fecha = $fecha;
        $movimiento->Cantidad = $cantidad;
        $movimiento->TipoMovimiento = "entrada";
        $movimiento->Stock = $inventario->Stock;
        $movimiento->SubtipoMovimiento = 0;
        $movimiento->IdSubtipoMovimiento = 0;
        $movimiento->save();

        $saldo_anterior = Caja::all()->last()->Saldo;
        $salida_caja = new Caja;
        $salida_caja->fechamovimiento = $fecha;
        $salida_caja->descripcion = "compra de ".$inventario->Descripcion;
        $salida_caja->movimiento = "salida";
        $salida_caja->monto = $monto;
        $salida_caja->saldo = $saldo_anterior-$monto;
        $salida_caja->save();

        return "";

    }

    public function salida_venta_directa(Request $request){
        
    	$id_repuesto = $request->input('Repuesto');
    	$cantidad = $request->input('Cantidad');
        $fecha = Carbon::now();
    	$id_cliente = $request->input('Cliente');
        $monto = $request->input('PrecioUnitario')*$cantidad;
        $motivo = $request->input('Motivo');

        $cantidad_disponible = Inventario::findOrFail($id_repuesto)->Stock;
        if(($cantidad_disponible-$cantidad)<0){
            return response()->json(['mensaje'=>'Stock no disponible']);
        }
        
    	$salida = new SalidaVentaRepuesto;
        $salida->Motivo = $motivo;
        $salida->IdRepuesto = $id_repuesto;
        $salida->PrecioUnitario = $request->input('PrecioUnitario');
        //if($id_cliente)
        //$salida->IdCliente = $request->input('Cliente');
        $salida->save();

        $inventario = Inventario::findOrFail($id_repuesto);
        $inventario->Stock = $inventario->Stock - $cantidad;
        $inventario->update();

        $movimiento = new Movimiento;
        $movimiento->IdRepuesto = $salida->IdRepuesto;
        $movimiento->Fecha = $fecha;
        $movimiento->Cantidad = $cantidad;
        $movimiento->TipoMovimiento = "salida";
        $movimiento->Stock = $inventario->Stock;
        $movimiento->SubtipoMovimiento = 0;
        $movimiento->IdSubtipoMovimiento = 0;
        $movimiento->save();

        $saldo_anterior = Caja::all()->last()->Saldo;
        $salida_caja = new Caja;
        $salida_caja->fechamovimiento = $fecha;
        $salida_caja->descripcion = $motivo." ".$inventario->Descripcion;
        $salida_caja->movimiento = "entrada";
        $salida_caja->monto = $monto;
        $salida_caja->saldo = $saldo_anterior+$monto;
        $salida_caja->save();
        
        return response()->json(['mensaje'=>'Registrado']);

    } 

    public function store(Request $request){

        $fecha = Carbon::now();
        $cantidad = $request->input('cantidad');
        $precio_venta = $request->input('precio_venta');
        $descripcion = $request->input('descripcion');
        $id_categoria = $request->input('idcategoria');
        $id_um = $request->input('idum');
        $proveedor = $request->input('Proveedor');
        $monto = $precio_venta*$cantidad;

        $inventario = $this->guardar_inventario($descripcion,$id_categoria,$id_um,$cantidad,$precio_venta);
        $salida_caja = $this->guardar_salida_caja($fecha,$inventario,$monto);
        $entrada =  $this->guardar_entrada_repuesto($proveedor,$inventario,$precio_venta);
        $movimiento = $this->guardar_movimiento($entrada,$fecha,$cantidad,$inventario);
        
        return "";
        
    }

    public function guardar_inventario($descripcion,$id_categoria,$id_um,$cantidad,$precio_venta){
        
        $inventario = new Inventario;
        $inventario->Descripcion = $descripcion;
        $inventario->IdCategoria = $id_categoria;
        $inventario->IdUM = $id_um;
        $inventario->Stock = $cantidad;
        $inventario->PrecioVenta = $precio_venta;
        $inventario->Estado = '1';
        $inventario->save();

        return $inventario;
    }

    public function guardar_salida_caja($fecha,$inventario,$monto){
        
        $saldo_anterior = Caja::all()->last()->Saldo;
        $salida_caja = new Caja;
        $salida_caja->fechamovimiento = $fecha;
        $salida_caja->descripcion = "compra de ".$inventario->Descripcion;
        $salida_caja->movimiento = "salida";
        $salida_caja->monto = $monto;
        $salida_caja->saldo = $saldo_anterior-$monto;
        $salida_caja->save();

        return $salida_caja;
    }

    public function guardar_entrada_repuesto($proveedor,$inventario,$precio_venta){
        
        $entrada = new EntradaRepuesto;
        $entrada->RucProveedor = $proveedor;
        $entrada->IdRepuesto = $inventario->Id;
        $entrada->PrecioUnitario = $precio_venta;
        $entrada->save();

        return $entrada;
    }

    public function guardar_movimiento($entrada,$fecha,$cantidad,$inventario){

        $movimiento = new Movimiento;
        $movimiento->IdRepuesto = $entrada->IdRepuesto;
        $movimiento->Fecha = $fecha;
        $movimiento->Cantidad = $cantidad;
        $movimiento->TipoMovimiento = "entrada";
        $movimiento->Stock = $inventario->Stock;
        $movimiento->SubtipoMovimiento = 0;
        $movimiento->IdSubtipoMovimiento = 0;
        $movimiento->save();

        return $movimiento;
    }

    public function show($id){

    	$movimientos = DB::table('Movimiento')->where('IdRepuesto','=',$id)->select('Fecha','TipoMovimiento','Cantidad','Stock')->orderBy('Fecha','desc')->paginate(10);
    	$repuesto = Inventario::findOrFail($id);

    	return view('inventario.movimientos')->with("movimientos",$movimientos)->with("repuesto",$repuesto);

    }

    public function search(Request $request){

        $term = $request->term;
        $repuestos = DB::table('Inventario')->where('Descripcion','LIKE','%'.$term.'%')->get();

        if(count($repuestos) == 0){
            $searchResult[] = 'No encontrado';
        }else{
            foreach ($repuestos as $key => $value) {
                $searchResult[] = $value->Id." - ".$value->Descripcion." - ".$value->PrecioVenta;
            }
        }

        return $searchResult;

    }

}
