<?php

namespace Sistema\Http\Controllers;

use Illuminate\Http\Request;

use Sistema\Http\Requests;

use Sistema\Repuesto;
use Sistema\Inventario;
use Sistema\Caja;
use Sistema\EntradaRepuesto;
use Sistema\Movimiento;

use Carbon\Carbon;

use Illuminate\Support\Facades\Redirect;

use Sistema\Http\Requests\RepuestoFormRequest;

use DB;

class RepuestoController extends Controller
{

    public function __construct(){

    }

    public function index(Request $request){

        if($request){
            $query = trim($request->get('searchText'));
            $repuestos = DB::table('Repuesto as r')->join('Categoria as c','r.IdCategoria','=','c.Id')->join('UnidadMedida as u','r.IdUnidadMedida','=','u.Id')->select('r.Id', 'r.Descripcion', 'c.Descripcion as Categoria','u.Descripcion as UnidadMedida','r.Cantidad','r.PrecioVenta')->where('r.Descripcion','LIKE','%'.$query.'%')->where('r.Estado','=','1')->paginate(10);
            return view('almacen.repuesto.index',["repuestos"=>$repuestos,"searchText"=>$query]);
        }
    }

    public function create(){

        $categorias = DB::table('Categoria')->get();
        $unidades_medida = DB::table('UnidadMedida')->get();
        $proveedores = DB::table('Proveedor')->where('Estado','=','1')->get();

        return view("almacen.repuesto.create",["categorias"=>$categorias,"unidades_medida"=>$unidades_medida,"proveedores"=>$proveedores]);

    }

    public function store(Request $request){
        $repuesto = new Repuesto;


        $repuesto->Descripcion = $request->get('descripcion');
        $repuesto->IdCategoria = $request->get('idcategoria');
        $repuesto->IdUnidadMedida = $request->get('idum');
        $repuesto->Cantidad = $request->get('cantidad');
        $repuesto->PrecioVenta = $request->get('precio_venta');
        $repuesto->Estado = '1';
        $repuesto->save();
        
        return Redirect::to('almacen/repuesto');
        
    }

    public function modal_store(Request $request){

        $fecha = Carbon::now();
        $cantidad = $request->input('cantidad');
        $precio_venta = $request->input('precio_venta');
        $monto = $precio_venta*$cantidad;

        $inventario = new Inventario;
        $inventario->Descripcion = $request->get('descripcion');
        $inventario->IdCategoria = $request->get('idcategoria');
        $inventario->IdUM = $request->get('idum');
        $inventario->Stock = $cantidad;
        $inventario->PrecioVenta = $precio_venta;
        $inventario->Estado = '1';
        $inventario->save();

        $saldo_anterior = Caja::all()->last()->Saldo;
        $salida_caja = new Caja;
        $salida_caja->fechamovimiento = $fecha;
        $salida_caja->descripcion = "compra de ".$inventario->Descripcion;
        $salida_caja->movimiento = "salida";
        $salida_caja->monto = $monto;
        $salida_caja->saldo = $saldo_anterior-$monto;
        $salida_caja->save();

        $entrada = new EntradaRepuesto;
        $entrada->RucProveedor = $request->input('Proveedor');
        $entrada->IdRepuesto = $inventario->Id;
        $entrada->PrecioUnitario = $precio_venta;
        $entrada->save();

        $movimiento = new Movimiento;
        $movimiento->IdRepuesto = $entrada->IdRepuesto;
        $movimiento->Fecha = $fecha;
        $movimiento->Cantidad = $cantidad;
        $movimiento->TipoMovimiento = "entrada";
        $movimiento->Stock = $inventario->Stock;
        $movimiento->SubtipoMovimiento = 0;
        $movimiento->IdSubtipoMovimiento = 0;
        $movimiento->save();

        return "";
        
    }

    

    public function show(){


    }

    public function edit($id){
        $repuesto = Repuesto::findOrFail($id);
        $categorias = DB::table('Categoria')->where('Estado','=','1')->get();
        $unidades_medida = DB::table('UnidadMedida')->get();
        $proveedores = DB::table('Proveedor')->where('Estado','=','1')->get();
        
        return view("almacen.repuesto.edit",["repuesto"=>$repuesto,"categorias"=>$categorias,"unidades_medida"=>$unidades_medida,"proveedores"=>$proveedores]);

    }

    public function update(RepuestoFormRequest $request,$id){
        $repuesto = Repuesto::findOrFail($id);
        $repuesto->Descripcion = $request->get('descripcion');
        $repuesto->IdCategoria = $request->get('idcategoria');
        $repuesto->IdUnidadMedida = $request->get('idum');
        $repuesto->Cantidad = $request->get('cantidad');
        $repuesto->PrecioVenta = $request->get('precioventa');
        $repuesto->update();
        return Redirect::to('almacen/repuesto');

    }

    public function destroy($id){

        $repuesto = Categoria::findOrFail($id);
        $repuesto->Estado = '0';
        $repuesto->update();
        return Redirect::to('almacen/repuesto');

    }

    public function search(Request $request){

        $term = $request->term;
        $repuestos = DB::table('Repuesto')->where('Descripcion','LIKE','%'.$term.'%')->get();

        if(count($repuestos) == 0){
            $searchResult[] = 'No encontrado';
        }else{
            foreach ($repuestos as $key => $value) {
                $searchResult[] = $value->Id." - ".$value->Descripcion." - ".$value->PrecioVenta;

            }
        }

        return $searchResult;

    }

}
