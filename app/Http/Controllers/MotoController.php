<?php

namespace Sistema\Http\Controllers;

use Illuminate\Http\Request;

use Sistema\Http\Requests;

use Sistema\Moto;

use Illuminate\Support\Facades\Redirect;
use Sistema\Http\Requests\MotoFormRequest;

use DB;

class MotoController extends Controller
{
    public function __construct(){

    }

    public function index(Request $request){

    }

    public function search(Request $request){

        $term = $request->term;
        $motos = DB::table('Moto')->where('Placa','LIKE','%'.$term.'%')->get();
        
        if(count($motos) == 0){
            $searchResult[] = 'No encontrado';
        }else{
            foreach ($motos as $key => $value) {
                $searchResult[] = $value->Placa." - ".$value->Marca." - ".$value->Color;

            }
        }

        return $searchResult;
        
    }

    public function store(MotoFormRequest $request){

        

        $moto = new Moto;

        $moto->create($request->all()); 
          
        return response()->json();
        
        
    }
}
