<?php

namespace Sistema\Http\Controllers;

use Illuminate\Http\Request;

use Sistema\Http\Requests;

use Sistema\Categoria;

use Illuminate\Support\Facades\Redirect;

use Sistema\Http\Requests\CategoriaFormRequest;

use DB;

class CategoriaController extends Controller
{
    
    public function __construct(){


    }

    public function index(Request $request){

    	if($request){
    		$query = trim($request->get('searchText'));
    		$categorias = DB::table('Categoria')->where('Descripcion','LIKE','%'.$query.'%')->where('Estado','=','1')->paginate(10);
    		return view('almacen.categoria.index',["categorias"=>$categorias,"searchText"=>$query]);
    	}
    }

    public function create(){

        
    	return view("almacen.categoria.create");

    }

    public function store(CategoriaFormRequest $request){

    	$categoria = new Categoria;
    	$categoria->Descripcion = $request->get('descripcion');
    	$categoria->Estado = '1';
    	$categoria->save();
    	return Redirect::to('almacen/categoria');

    }

    public function show($id){

    	return view("almacen.categoria.show",["categoria"=>Categoria::findOrFail($id)]);

    }

	public function edit($id){

		return view("almacen.categoria.edit",["categoria"=>Categoria::findOrFail($id)]);
    }

	public function update(CategoriaFormRequest $request,$id){

		$categoria = Categoria::findOrFail($id);
		$categoria->Descripcion = $request->get('descripcion');
		$categoria->update();
		return Redirect::to('almacen/categoria');

    }

	public function destroy($id){

		$categoria = Categoria::findOrFail($id);
		$categoria->Estado = '0';
		$categoria->update();
		return Redirect::to('almacen/categoria');

    }



}
