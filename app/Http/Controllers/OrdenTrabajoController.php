<?php

namespace Sistema\Http\Controllers;

use Illuminate\Http\Request;
use Sistema\Http\Requests;

use Sistema\OrdenTrabajo;
use Sistema\OrdenTrabajoDetalle;
use Sistema\Cliente;
use Sistema\Moto;
use Sistema\User;
use Sistema\Estado;
use Sistema\Inventario;
use Sistema\Movimiento;
use Carbon\Carbon;
use DB;
use PDF;

use Illuminate\Support\Facades\Redirect;
use Sistema\Http\Requests\OrdenTrabajoFormRequest;

class OrdenTrabajoController extends Controller
{

    public function __construct(){


    }

    public function index(Request $request){

        if($request){
            $query = trim($request->get('searchText'));
            $trabajos = DB::table('OrdenTrabajo as ot')->join('Cliente as c','ot.IdCliente','=','c.Id')->join('Moto as m','ot.Placa','=','m.Placa')->join('Estado as e','ot.Estado','=','e.Id')->select('ot.Id','c.Nombres as Cliente','m.Modelo as Modelo','m.Marca as Marca','ot.Placa','ot.DescripcionServicio','ot.FechaEntrada','e.Id as IdEstado','e.Descripcion as Estado')->orWhere('ot.Id','LIKE','%'.$query.'%')->orWhere('m.Placa','LIKE','%'.$query.'%')->orWhere('c.Nombres','LIKE','%'.$query.'%')->paginate(10);
                
            return view('servicio.index')->with("trabajos",$trabajos)->with("searchText",$query);
        }
    }

    public function create(){

        $estados = DB::table('Estado')->get();
        $categorias = DB::table('Categoria')->get();
        $unidades_medida = DB::table('UnidadMedida')->get();
        $proveedores = DB::table('Proveedor')->get();

        return view("servicio.create")->with("estados",$estados)->with("categorias",$categorias)->with("unidades_medida",$unidades_medida)->with("proveedores",$proveedores);

    }

    public function store(Request $request){


        $trabajo = new OrdenTrabajo;

        $trabajo->FechaEntrada = Carbon::createFromFormat('Y-m-d H:i', $request->get('fecha_entrada')." ".$request->get('hora_entrada'));

        $trabajo->DescripcionServicio = $request->get('descripcion_servicio');
        $trabajo->TrabajosRealizados = $request->get('trabajos_realizados');
        $trabajo->Estado = $request->get('id_estado');
        $cliente = $request->get('cliente');
        $temp = substr($cliente,0, strpos($cliente,'-')-1);
        $trabajo->IdCliente = $temp;

        $moto = $request->get('moto');
        //$temp = substr($moto,0,strpos($moto,"-")-1);

        $temp = substr($moto,0,7);

        $trabajo->Documento = $request->get("documento");

        $trabajo->Placa = $temp;
        $trabajo->save();

        return Redirect::to('servicio');

    }

    public function ajax_store(Request $request){

        $datos = $request->input("data");
        $posicion = strpos($datos,'-')-1;
        $id_repuesto = substr($datos,0,$posicion);
        
        $id_orden_trabajo = $request->input("id_orden_trabajo");


        $repuesto = Inventario::findOrFail($id_repuesto);
        
        $temporal = DB::table('OrdenTrabajoDetalle')->where('IdOrdenTrabajo','=',$id_orden_trabajo)->where('CodigoRepuesto','=',$id_repuesto)->first();

        

        if($temporal!=null){

            $orden_trabajo_detalle = OrdenTrabajoDetalle::findOrFail($temporal->Id); 
            $orden_trabajo_detalle->Cantidad = $orden_trabajo_detalle->Cantidad + 1;
            $orden_trabajo_detalle->Monto = $orden_trabajo_detalle->Monto + $repuesto->PrecioVenta;
            $orden_trabajo_detalle->update();

        }else{

            $orden_trabajo_detalle = new OrdenTrabajoDetalle;
            $orden_trabajo_detalle->IdOrdenTrabajo = $id_orden_trabajo;
            $orden_trabajo_detalle->CodigoRepuesto = $id_repuesto;
            $orden_trabajo_detalle->Cantidad = 1;
            $orden_trabajo_detalle->Monto = $repuesto->PrecioVenta;
            $orden_trabajo_detalle->save();            

        }

        $inventario = $this->actualizar_inventario($repuesto->Id,-1);
        $this->guardar_movimiento($repuesto->Id,1,$inventario,"salida");

        $repuesto_imprimible = [
            "cantidad" => $orden_trabajo_detalle->Cantidad,
            "descripcion" => $repuesto->Descripcion,
            "monto" => $orden_trabajo_detalle->Monto,
        ];
        return $repuesto_imprimible;

    }

    public function actualizar_inventario($id_repuesto,$cantidad){

        $inventario = Inventario::findOrFail($id_repuesto);
        $inventario->Stock = $inventario->Stock+$cantidad;
        $inventario->update();

        return $inventario;
    }

    public function ajax_actualizar_cantidad_orden_trabajo_detalle(Request $request){


        $id_repuesto = $request->input("id_repuesto");
        $id_orden_trabajo = $request->input("id_orden_trabajo");
        $cantidad = $request->input("cantidad");

        $repuesto = Inventario::findOrFail($id_repuesto);
        $temporal = DB::table('OrdenTrabajoDetalle')->where('IdOrdenTrabajo','=',$id_orden_trabajo)->where('CodigoRepuesto','=',$id_repuesto)->first();
        

        $orden_trabajo_detalle = OrdenTrabajoDetalle::findOrFail($temporal->Id);
        $cantidad_anterior = $orden_trabajo_detalle->Cantidad;
        $orden_trabajo_detalle->Cantidad = $cantidad; 
        $orden_trabajo_detalle->Monto = $cantidad*$repuesto->PrecioVenta;
        $orden_trabajo_detalle->update();

        $inventario = $this->actualizar_inventario($id_repuesto,$cantidad_anterior-$cantidad);
        $this->guardar_movimiento($repuesto->Id,abs($cantidad_anterior-$cantidad),$inventario,"salida");


        return response()->json();
    }

    public function guardar_movimiento($id_repuesto,$cantidad,$inventario,$tipo_movimiento){
        $movimiento = new Movimiento;
        $movimiento->IdRepuesto = $id_repuesto;
        $movimiento->Fecha = Carbon::now();
        $movimiento->Cantidad = $cantidad;
        $movimiento->TipoMovimiento = $tipo_movimiento;
        $movimiento->Stock = $inventario->Stock;
        $movimiento->SubtipoMovimiento = 0;
        $movimiento->IdSubtipoMovimiento = 0;
        $movimiento->save();
    }

    public function lista_repuestos_ajax($id){

        $lista_repuestos = DB::table("OrdenTrabajoDetalle as otd")->where("IdOrdenTrabajo","=",$id)->join("Inventario as i","otd.CodigoRepuesto","=","i.Id")->select("i.Id","otd.Cantidad","i.Descripcion","otd.Monto")->get();

        return $lista_repuestos;
    }

    public function ajax_remove_orden_trabajo_detalle(Request $request){

        $id_orden_trabajo = $request->input("id_orden_trabajo");
        $id_repuesto = $request->input("id_repuesto");

        $orden_trabajo_detalle = DB::table("OrdenTrabajoDetalle")->where("IdOrdenTrabajo","=",$id_orden_trabajo)->where("CodigoRepuesto","=",$id_repuesto)->first();
        

        $inventario = $this->actualizar_inventario($id_repuesto,$orden_trabajo_detalle->Cantidad);
        $this->guardar_movimiento($id_repuesto,$orden_trabajo_detalle->Cantidad,$inventario,"entrada");

        OrdenTrabajoDetalle::destroy($orden_trabajo_detalle->Id);

        return response()->json();
    }

    public function show($id){

        $trabajo = OrdenTrabajo::findOrFail($id);
        $cliente = Cliente::findOrFail($trabajo->IdCliente);
        $moto = Moto::findOrFail($trabajo->Placa);
        $pdf = PDF::loadView('vista',['trabajo'=>$trabajo,'cliente'=>$cliente,'moto'=>$moto]);
        return $pdf->stream('OT.pdf'); 

    }

    public function edit($id){

        $trabajo = OrdenTrabajo::findOrFail($id);
        $cliente = Cliente::findOrFail($trabajo->IdCliente);
        $categorias = DB::table('Categoria')->get();
        $unidades_medida = DB::table('UnidadMedida')->get();
        $proveedores = DB::table('Proveedor')->get();
        $repuestos = DB::table('OrdenTrabajoDetalle as otd')->join('Inventario as r','otd.CodigoRepuesto','=','r.Id')->select('r.Id','otd.CodigoRepuesto','otd.Cantidad','r.Descripcion as Descripcion','otd.Monto','r.PrecioVenta as PrecioVenta')->where('otd.IdOrdenTrabajo','=',$trabajo->Id)->get();
        
        $moto = DB::table('Moto')->where('Placa','=',$trabajo->Placa)->first();
        //$moto = Moto::findOrFail($trabajo->Placa)->Placa;
        //dd($moto);
        $estados = Estado::all();
        


        return view("servicio.edit",["trabajo"=> $trabajo,"cliente"=>$cliente,"repuestos"=>$repuestos,"estados"=>$estados])->with("categorias",$categorias)->with("unidades_medida",$unidades_medida)->with("proveedores",$proveedores)->with("moto",$moto);

    }

    public function update(OrdenTrabajoFormRequest $request,$id){


        $trabajo = OrdenTrabajo::findOrFail($id);

        $trabajo->Estado = $request->get('id_estado');
        $trabajo->DescripcionServicio = $request->get('descripcion_servicio');
        $trabajo->TrabajosRealizados = $request->get('trabajos_realizados');
        $trabajo->FechaSalida = Carbon::createFromFormat('Y-m-d H:i', $request->get('fecha_salida')." ".$request->get('hora_salida'));
        $trabajo->update();

        return Redirect::to('servicio');

    }

    public function destroy($id){

        $trabajo = OrdenTrabajo::findOrFail($id);
        $trabajo->Estado = '0';
        $trabajo->update();
        return Redirect::to('servicio');

    }

}
