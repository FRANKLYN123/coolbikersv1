<?php

namespace Sistema\Http\Controllers;

use Illuminate\Http\Request;

use Sistema\Http\Requests;

use Sistema\OrdenTrabajo;
use Sistema\OrdenTrabajoDetalle;

use Sistema\Cliente;
use Sistema\Moto;
use Sistema\User;
use Sistema\Estado;
use Sistema\Repuesto;


use Illuminate\Support\Facades\Redirect;

use Carbon\Carbon;

use DB;

require __DIR__ . '/ticket/autoload.php'; //Nota: si renombraste la carpeta a algo diferente de "ticket" cambia el nombre en esta línea
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;


class TicketController extends Controller
{
	public function __construct(){


	}
	public function index(){
		
	}

	public function show($id){


		
		$trabajo = OrdenTrabajo::findOrFail($id);

		$moto = DB::table('Moto')->where('Placa','=',$trabajo->Placa)->first();
		
		
		$cliente = Cliente::findOrFail($trabajo->IdCliente);

		$nombre_impresora = "TermicaImpresora"; 

		$connector = new WindowsPrintConnector($nombre_impresora);
		$printer = new Printer($connector);

		
		$printer->setJustification(Printer::JUSTIFY_CENTER);

		$logo = EscposImage::load("logo_bw.png", false);
		$printer->bitImage($logo);


		$printer->text("\n"."Cool Bicker's S.R.L." . "\n");
		$printer->text("Av. Jesus 816 Urb. 15 de Enero - Paucarpata - Arequipa" . "\n");
		$printer->text("Tel: 054-323708" . "\n");

		$printer->setJustification(Printer::JUSTIFY_LEFT);
		
		$printer->text("-------------------------------"."\n");
		
		$printer->setJustification(Printer::JUSTIFY_LEFT);
		$printer->text( "OT:          ");
		$printer->text($trabajo->Id."\n");
		$printer->text( "Placa:       ");
		$printer->text($moto->Placa."\n");
		$printer->text( "Marca:       ");
		$printer->text($moto->Marca."\n");
		$printer->text( "Kilometraje: ");
		$printer->text($moto->Km." Km\n");

		$printer->text( "Cliente: ");
		//$printer->setPrintLeftMargin(32);
		$printer->text($cliente->Nombres."\n");
		//$printer->setPrintLeftMargin(0);

		$printer->text("Fecha de Entrada: ");
		$printer->text($trabajo->FechaEntrada->format("d-m-Y")."\n");
		$printer->text("Hora de Entrada:  ");
		$printer->text($trabajo->FechaEntrada->format("H:i")."\n");
		$printer->text("Servicio: ");

		//$printer->setPrintLeftMargin(32);
		$printer->text($trabajo->DescripcionServicio."\n");
		//$printer->setPrintLeftMargin(0);

		$printer->setJustification(Printer::JUSTIFY_LEFT);
		$printer->text("-------------------------------"."\n");
		$printer->setJustification(Printer::JUSTIFY_CENTER);
		$printer->text("Al servicio de tu pasion\n");

		$printer->feed(5);

		
		
		$printer->close();
		return Redirect::back();
	}

}
