<?php

namespace Sistema\Http\Controllers;

use Illuminate\Http\Request;

use Sistema\Http\Requests;

use Sistema\UnidadMedida;

use Illuminate\Support\Facades\Redirect;

use Sistema\Http\Requests\UnidadMedidaFormRequest;

use DB;

class UnidadMedidaController extends Controller
{
	public function __construct(){


	}

	public function index(Request $request){

		if($request){
			$query = trim($request->get('searchText'));
			$unidades_medida = DB::table('UnidadMedida')->where('Descripcion','LIKE','%'.$query.'%')->paginate(10);
			return view('almacen.unidad_medida.index',["unidades_medida"=>$unidades_medida,"searchText"=>$query]);
		}
	}

	public function create(){


		return view("almacen.unidad_medida.create");

	}

	public function store(UnidadMedidaFormRequest $request){

		$unidad_medida = new UnidadMedida;
		$unidad_medida->Descripcion = $request->get('descripcion');
		$unidad_medida->save();
		return Redirect::to('almacen/unidad_medida');

	}

	public function show($id){

	}

	public function edit($id){

		return view("almacen.unidad_medida.edit",["unidad_medida"=>UnidadMedida::findOrFail($id)]);
	}

	public function update(Request $request,$id){

		$unidad_medida = UnidadMedida::findOrFail($id);
		$unidad_medida->Descripcion = $request->get('descripcion');
		$unidad_medida->update();
		return Redirect::to('almacen/unidad_medida');

	}

	public function destroy($id){

		UnidadMedida::destroy($id);
		return Redirect::to('almacen/unidad_medida');

	}

}
