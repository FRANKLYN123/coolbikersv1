<?php

namespace Sistema\Http\Controllers;

use Illuminate\Http\Request;

use Sistema\Http\Requests;

use Sistema\Cliente;

use Illuminate\Support\Facades\Redirect;

use Sistema\Http\Requests\ClienteFormRequest;
use Sistema\Http\Requests\ClienteUpdateFormRequest;

use DB;

class ClienteController extends Controller
{
    public function __construct(){

    }

    public function index(Request $request){

        if($request){
            $query = trim($request->get('searchText'));
            $clientes = DB::table('Cliente')->where('Estado','=','1')->where('Id','LIKE','%'.$query.'%')->orWhere('Nombres','LIKE','%'.$query.'%')->where('Estado','=','1')->paginate(10);
           
            return view('cliente.index',["clientes"=>$clientes,"searchText"=>$query]);
        }
    }

    public function search(Request $request){

        $term = $request->term;
        $clientes = DB::table('Cliente')->where('Nombres','LIKE','%'.$term.'%')->orwhere('Id','LIKE','%'.$term.'%')->get();
        
        if(count($clientes) == 0){
            $searchResult[] = 'No encontrado';
        }else{
            foreach ($clientes as $key => $value) {
                $searchResult[] = $value->Id." - ".$value->Nombres;

            }
        }

        return $searchResult;

    }

    public function create(){

        return view('cliente.create');

    }


    public function store(ClienteFormRequest $request){
    
    	$cliente = new Cliente;

        $numero_documento = $request->get('numero_documento');

        $longitud_documento = strlen($numero_documento);
            
        if ($longitud_documento==8) {
            $cliente->TipoDocumento = 'DNI';
        }else if($longitud_documento==11){
            $cliente->TipoDocumento = 'RUC';
        }

        $cliente->Id = $numero_documento;
        $cliente->Nombres = $request->get('cliente');
        $cliente->Direccion = $request->get('direccion');
        $cliente->Correo = $request->get('correo');
        $cliente->Telefono = $request->get('telefono');     
        $cliente->Estado = '1';

        $cliente->save();   

        
        return response()->json();
        
    }

    public function show(){

    }

    public function edit($id){

        $cliente = Cliente::findOrFail($id);

        return view("cliente.edit")->with("cliente",$cliente);
    }

    public function update(ClienteUpdateFormRequest $request,$id){

        $cliente = Cliente::findOrFail($id);
        $cliente->Nombres = $request->get('cliente');
        $cliente->Direccion = $request->get('direccion');
        $cliente->Correo = $request->get('correo');
        $cliente->Telefono = $request->get('telefono');     
        $cliente->update();

        return Redirect::to('cliente/');
    }

    public function destroy($id){

        $cliente = Cliente::findOrFail($id);
        $cliente->Estado = '0';

        $cliente->update();

        return Redirect::to('cliente/');
    }

}
