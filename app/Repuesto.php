<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class Repuesto extends Model
{
    protected $table = 'Repuesto';

    protected $primaryKey = 'Id';

    public $timestamps = false;

    protected $fillable = [
    	
    	'Descripcion',
        'IdCategoria',
        'IdUnidadMedida',
        'Cantidad',
    	'PrecioVenta',
        'Estado'
    ];


    protected $guarded = [

    ];
    
}
