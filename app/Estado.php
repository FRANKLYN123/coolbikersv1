<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
	protected $table = 'Estado';

	protected $primaryKey = 'Id';

	public $timestamps = false;

	protected $fillable = [

		'Descripcion'

	]; 

	protected $guarded = [

	];
}
