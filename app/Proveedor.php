<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = 'Proveedor';

    protected $primaryKey = 'Ruc';

    public $timestamps = false;

    protected $fillable = [
    	'RazonSocial',
    	'DireccionFiscal',
    	'Sucursal',
    	'Telefono',
    	'Correo',
    	'PaginaWeb',
    	'Estado'
    ];


    protected $guarded = [

    ];
}
