<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'Cliente';

    protected $primaryKey = 'Id';

    public $timestamps = false;

    protected $fillable = [

    	'TipoDocumento',
    	'Nombres',
    	'Direccion',
    	'Correo',
    	'Telefono',
    	'Estado',

    ]; 

    protected $guarded = [

    ];
}
