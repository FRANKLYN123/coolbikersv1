<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    protected $table = 'Inventario';

    protected $primaryKey = 'Id';

    public $timestamps = false;

    protected $fillable = [
    	
    	'Descripcion',
        'IdCategoria',
        'IdUM',
    	'PrecioVenta',
    	'Stock',
        'Estado'
    ];


    protected $guarded = [

    ];
    
}
