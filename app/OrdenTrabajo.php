<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class OrdenTrabajo extends Model
{
    protected $table = 'OrdenTrabajo';

    protected $primaryKey = 'Id';

    public $timestamps = false;

    protected $fillable = [
        'Id',
    	'DniCliente',
    	'DniConductor',
    	'Placa',
    	'FechaEntrada',
    	'FechaSalida',
        'MarcasMoto',
    	'DescripcionServicio',
    	'TrabajosRealizados',
        'Documento',
    	'Estado'
    ];

    protected $guarded = [

    ];

    protected $dates = [
        'FechaEntrada',
        'FechaSalida'
    ];

}
