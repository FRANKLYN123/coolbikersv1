<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class EntradaRepuesto extends Model
{
    protected $table = 'entrada_repuesto';

    protected $primaryKey = 'Id';

    public $timestamps = false;

    protected $fillable = [

    	'RucProveedor',
    	'IdRepuesto',
    	'PrecioUnitario'

    ]; 

    protected $guarded = [

    ];
}
