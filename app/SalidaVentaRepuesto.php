<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class SalidaVentaRepuesto extends Model
{
    protected $table = 'Salida_Venta';

    protected $primaryKey = 'Id';

    public $timestamps = false;

    protected $fillable = [
    	'Motivo',
    	'IdRepuesto',
    	'PrecioUnitario',
    	'IdCliente'

    ]; 

    protected $guarded = [

    ];
}
