
$(document).ready(function() {
  $("#myImgId").bind("load", function(event) {
    var myImg = document.getElementById("myImgId");
    var h = document.getElementById('hiddenMotoMarcas');

    var div = document.getElementById('emptyPlanet');

    circle = document.getElementById("planet.1");       
    style = window.getComputedStyle(circle);
    width = parseInt(style.getPropertyValue('width'),10);

    var Coordinates = h.value.split(';');

    for (i = 0; i < Coordinates.length; i++) {
      var partsOfCoord = Coordinates[i].split(',');

      var ImgPos;
      ImgPos = FindPosition(myImg);

      var firstPosX = parseInt(partsOfCoord[0],10) + ImgPos[0];
      var firstPosY = parseInt(partsOfCoord[1],10) + ImgPos[1];

      div.innerHTML += '<div id="planet.'+i+'" class="circulo" style="opacity:0.8; left:'+firstPosX+'px;top:'+firstPosY+'px;""></div>';
    }
  });
});

function FindPosition(oElement)
{
  if(typeof( oElement.offsetParent ) != "undefined")
  {
    for(var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent)
    {
      posX += oElement.offsetLeft;
      posY += oElement.offsetTop;
    }
    return [ posX, posY ];
  }
  else
  {
    return [ oElement.x, oElement.y ];
  }
}

function GetCoordinates(e)
{
  var myImg = document.getElementById("myImgId");
  var PosX = 0;
  var PosY = 0;
  var ImgPos;
  ImgPos = FindPosition(myImg);
  if (!e) var e = window.event;
  if (e.pageX || e.pageY)
  {
    PosX = e.pageX;
    PosY = e.pageY;
  }
  else if (e.clientX || e.clientY)
  {
    PosX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    PosY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
  }
  var firstPosX = PosX;
  var firstPosY = PosY;
  PosX = PosX - ImgPos[0];
  PosY = PosY - ImgPos[1];
  document.getElementById("x").innerHTML = PosX;
  document.getElementById("y").innerHTML = PosY;

  circle = document.getElementById("planet.1");
  circle.style.opacity = 0.8;
  style = window.getComputedStyle(circle);
  width = parseInt(style.getPropertyValue('width'),10);

  firstPosX = PosX + ImgPos[0]-width/2;
  firstPosY = PosY + ImgPos[1]-width/2;
  circle.style.left = firstPosX+"px";
  circle.style.top = firstPosY+"px";

  //

  
}