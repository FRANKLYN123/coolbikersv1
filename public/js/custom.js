$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#modal_nuevo_movimiento').on('hidden.bs.modal', function(e)
{      
    $("#campo_respuesta_caja").attr("class","");
    $("#campo_respuesta_caja").html("");
}) ;


//INVENTARIO

//guardar entrada

$("#boton_guardar_entrada").click(function (e) {

    e.preventDefault();
    

    var data = {
        Repuesto:        $('#repuesto_entrada').val(),
        Proveedor:         $('#proveedor_entrada').val(),
        Cantidad:              $('#cantidad_entrada').val(),
        PrecioUnitario:   $('#precio_unitario_entrada').val(),
    }
    console.log(data);

    var campo_respuesta = $('#campo_respuesta_entrada_inventario');

    $.ajax({
        type: "get",
        url: "http://localhost:8000/inventario_entrada",
        data: data,
        success: function (data) {
            campo_respuesta.attr("class","alert alert-success");
            campo_respuesta.html("<p>Registrado</p>");
        },
        error: function (data) {            
            campo_respuesta.attr("class","alert alert-danger");
            campo_respuesta.html("");
            campo_respuesta.append("<p>Error</p>");          
        }

    });
});

//guardar salida por venta directa

$("#boton_guardar_salida_venta_directa").click(function (e) {

    e.preventDefault();

    var data = {
        Repuesto:        $('#repuesto_salida').val(),
        Cliente:         $('#cliente_salida').val(),
        Motivo:          $('#motivo_salida').val(),
        Cantidad:        $('#cantidad_salida').val(),
        PrecioUnitario:  $('#precio_unitario_salida').val(),
    }
    console.log(data);

    var campo_respuesta = $('#campo_respuesta_salida_inventario');

    $.ajax({
        type: "get",
        url: "http://localhost:8000/inventario_salida_venta_directa",
        data: data,
        dataType: 'json',
        success: function (data) {
            alert(data.mensaje);
        },
        error: function (data) {    
            alert("b");        
            campo_respuesta.attr("class","alert alert-danger");
            campo_respuesta.html("");
            campo_respuesta.append("<p>Error</p>");          
        }

    });
});





//MOTO

$("#boton_guardar_movimiento").click(function (e) {

    e.preventDefault();
    
    var data = {
        descripcion:        $('#descripcion').val(),
        movimiento:         $('#movimiento').val(),
        monto:              $('#monto').val(),
        fecha_movimiento:   $('#fecha_movimiento').val(),
    }
    console.log(data);
    var campo_respuesta = $("#campo_respuesta_caja");

    $.ajax({
        type: "post",
        url: "http://localhost:8000/caja",
        data: data,
        success: function (data) {
            campo_respuesta.attr("class","alert alert-success");
            campo_respuesta.html("<p>Registrado</p>");
        },
        error: function (data) {
            campo_respuesta.attr("class","alert alert-danger");
            campo_respuesta.html("");
            campo_respuesta.append("<p>Error</p>");            
        }

    });
});


$("#btn-guardar-moto").click(function (e) {

    e.preventDefault();
    
    var formData = {
        Placa:          $('#placa').val(),
        Marca:          $('#marca').val(),
        Color:          $('input[name=radio]:checked').val(),
        Modelo:         $('#modelo').val(),
        NumeroSerie:    $('#numero_serie').val(),
        NumeroMotor:    $('#numero_motor').val(),
        AñoFabricacion: $('#año_fabricacion').val(),
        Km:             $('#km').val(),

    }

    
    
    $.ajax({
        type: "post",
        url: "http://localhost:8000/moto",
        data: formData,
        dataType: 'json',
        success: function (data) {

            console.log(data);
            $("#campo_respuesta_moto").attr("class","alert alert-success");
            $("#campo_respuesta_moto").html("<p>Registrado</p>");

        },
        error: function (data) {

            $("#campo_respuesta_moto").attr("class","alert alert-danger");
            $("#campo_respuesta_moto").html("");
            $("#campo_respuesta_moto").append("<p>error</p>");
            if(data.responseJSON.Placa)
                $("#campo_respuesta_moto").append("<p>"+data.responseJSON.Placa+"</p>");
            
        }

    });
});

//CLIENTE

$("#btn-guardar-cliente").click(function (e) {

    e.preventDefault();
    var formData = {
        numero_documento:     $('#numero_documento').val(),
        cliente:        $('#nombre_cliente').val(),
        direccion:      $('#direccion').val(),
        correo:         $('#correo').val(),
        telefono:       $('#telefono').val(),
    }


    $.ajax({
        type: "post",
        url: "http://localhost:8000/cliente",
        data: formData,
        dataType: 'json',
        success: function (data) {

            $("#campo_respuesta_cliente").attr("class","alert alert-success");
            $("#campo_respuesta_cliente").html("<p>Registrado</p>");

            
        },
        error: function (data) {

            console.log(data);
            $("#campo_respuesta_cliente").attr("class","alert alert-danger");
            $("#campo_respuesta_cliente").html("");
            $("#campo_respuesta_cliente").append("<p>error</p>");
            if(data.responseJSON.numero_documento)
                $("#campo_respuesta_cliente").append("<p>"+data.responseJSON.numero_documento+"</p>");
            

            /*var a = JSON.parse(data.responseText);
            $("#campo_respuesta_cliente").attr("class","alert alert-danger");
            console.log(data.responseJSON['correo']);
            $("#campo_respuesta_cliente").html("<p>"+data.responseJSON.correo+"</p>");
            $("#campo_respuesta_cliente").append("<p>"+data.responseJSON.telefono+"</p>");
            */
        }

    });
});

$("#boton_cerrar_modal_cliente").on("click",function(e){
    $("#campo_respuesta").attr("class","");
    $("#campo_respuesta").html(""); 
    e.preventDefault(); 
    $('#formulario_cliente_nuevo_ajax').trigger("reset"); 
});







//NUEVO REPUESTO

$("#btn-guardar-repuesto").click(function (e) {

    e.preventDefault();

    var formData = {

        descripcion:          $('#descripcion').val(),
        idcategoria:          $('#categoria').val(),
        idum:         $('#um').val(),
        cantidad:    $('#cantidad').val(),
        precio_venta:    $('#precio_venta').val(),
        Proveedor:       $('#proveedor').val(),

    }

    $.ajax({
        type: "post",
        url: "http://localhost:8000/inventario",
        data: formData,

        success: function (data) {

            $("#success-repuesto").html("<p class='text-success'>Registrado</p>");
            
        },
        error: function (data) {

        }

    });
});



var i=1;  

// Guardar repuesto seleccionado en orden de trabajo detalle

$("#btn-agregar-repuesto").click(function (e) {

    e.preventDefault();

    var formData = {

        data:                   $('#repuesto').val(),
        id_orden_trabajo:       $('#ot_id').val(),
    } 

    var type = "get"; 
    console.log(formData);

    $.ajax({
        type: type,
        url: "http://localhost:8000/repuesto_ajax",
        data: formData,
        dataType: 'json',

        success: function (data) {

            $.ajax({
                type: type,
                url: "http://localhost:8000/lista_repuestos/"+$('#ot_id').val(),
                type: "get",
                success: function (data) {
                    $('#lista-repuestos tbody').empty();
                    var tamaño = Object.keys(data).length;
                    for (var i = 0; i < tamaño; i++) {
                        var id = data[i].Id;
                        var cantidad = data[i].Cantidad;
                        var descripcion = data[i].Descripcion;
                        var monto = data[i].Monto;
                        
                        var id_repuesto = "<input type='hidden' name='id[]' id='id_repuesto_"+(i)+"' value='"+id+"'>";
                        var cantidad = "<input type='text' name='cantidad[]' id='cantidad_"+(i)+"' class='form-control input_cantidad' value='"+cantidad+"'>";
                        var descripcion = "<input type='text' class='form-control' value='"+descripcion+"' disabled>";
                        var precio_venta = "<input type='text' name='precio_venta[]' id='precio_venta"+(i)+"'  class='form-control' value='"+monto+"'>";
                        var boton = "<button id='"+(i)+"' class='btn btn-danger btn_eliminar_repuesto'>X</button>";

                        $('#lista-repuestos').append("<tr id='row"+(i)+"'><td>"+id_repuesto+cantidad+"</td><td>"+descripcion+"</td><td>"+precio_venta+"</td><td>"+boton+"</td></tr>");

                    }

                },
                error: function (data) {
                    console.log('Error:', data);
                }

            });
            //console.log(data);
        },
        error: function (data) {
            //console.log('Error:', data);
        }

    });

});

$(document).on('click', '.btn_eliminar_repuesto', function(e){

    e.preventDefault();
    var fila = $(this).attr("id");

    var formData = {
        id_orden_trabajo:           $('#ot_id').val(),
        id_repuesto:                $('#id_repuesto_'+fila).val(),
    }   

    var type = "get"; 
    console.log(formData);

    $.ajax({
        type: type,
        url: "http://localhost:8000/eliminar_orden_detalle_ajax",
        data: formData,
        dataType: 'json',
        success: function (data) {
            $.ajax({
                type: type,
                url: "http://localhost:8000/lista_repuestos/"+$('#ot_id').val(),
                type: "get",
                success: function (data) {
                    $('#lista-repuestos tbody').empty();
                    var tamaño = Object.keys(data).length;
                    for (var i = 0; i < tamaño; i++) {
                        var id = data[i].Id;
                        var cantidad = data[i].Cantidad;
                        var descripcion = data[i].Descripcion;
                        var monto = data[i].Monto;
                        
                        var id_repuesto = "<input type='hidden' name='id[]' id='id_repuesto_"+(i)+"' value='"+id+"'>";
                        var cantidad = "<input type='number' name='cantidad[]' id='cantidad_"+(i)+"' class='form-control input_cantidad' value='"+cantidad+"'>";
                        var descripcion = "<input type='text' class='form-control' value='"+descripcion+"' disabled>";
                        var precio_venta = "<input type='number' name='precio_venta[]' id='precio_venta"+(i)+"'  class='form-control' value='"+monto+"'>";
                        var boton = "<button id='"+(i)+"' class='btn btn-danger btn_eliminar_repuesto'>X</button>";

                        $('#lista-repuestos').append("<tr id='row"+(i)+"'><td>"+id_repuesto+cantidad+"</td><td>"+descripcion+"</td><td>"+precio_venta+"</td><td>"+boton+"</td></tr>");

                    }

                },
                error: function (data) {
                    console.log('Error:', data);
                }

            });
        },
        error: function (data) {
            console.log('Error:',"no");
        }
    });
});

$(document).on('change', '.input_cantidad', function(e){  

    e.preventDefault();
    var fila = $(this).attr("id").substring(9);
    
    var formData = {

        id_orden_trabajo:           $('#ot_id').val(),
        id_repuesto:                $('#id_repuesto_'+fila).val(),
        cantidad:                   $('#cantidad_'+fila).val(),

    }   



    var type = "get"; 
    console.log(formData);

    $.ajax({
        type: type,
        url: "http://localhost:8000/actualizar_cantidad_orden_detalle_ajax",
        data: formData,
        dataType: 'json',
        success: function (data) {
            $.ajax({
                type: type,
                url: "http://localhost:8000/lista_repuestos/"+$('#ot_id').val(),
                type: "get",
                success: function (data) {
                    $('#lista-repuestos tbody').empty();
                    var tamaño = Object.keys(data).length;
                    for (var i = 0; i < tamaño; i++) {
                        var id = data[i].Id;
                        var cantidad = data[i].Cantidad;
                        var descripcion = data[i].Descripcion;
                        var monto = data[i].Monto;
                        
                        var id_repuesto = "<input type='hidden' name='id[]' id='id_repuesto_"+(i)+"' value='"+id+"'>";
                        var cantidad = "<input type='number' name='cantidad[]' id='cantidad_"+(i)+"' class='form-control input_cantidad' value='"+cantidad+"'>";
                        var descripcion = "<input type='text' class='form-control' value='"+descripcion+"' disabled>";
                        var precio_venta = "<input type='number' name='precio_venta[]' id='precio_venta"+(i)+"'  class='form-control' value='"+monto+"'>";
                        var boton = "<button id='"+(i)+"' class='btn btn-danger btn_eliminar_repuesto'>X</button>";

                        $('#lista-repuestos').append("<tr id='row"+(i)+"'><td>"+id_repuesto+cantidad+"</td><td>"+descripcion+"</td><td>"+precio_venta+"</td><td>"+boton+"</td></tr>");

                    }

                },
                error: function (data) {
                    console.log('Error:', data);
                }

            });
        },
        error: function (data) {
            console.log('Error:',"no");
        }
    });
    
});


$( function() {

    $( "#cliente" ).autocomplete({
      source: 'http://localhost:8000/searchCliente'
  });

    $( "#moto" ).autocomplete({
      source: 'http://localhost:8000/searchMoto'
  });

    $("#repuesto").autocomplete({

        source: 'http://localhost:8000/searchRepuesto',
        autoFocus: true,

        select: function(e,ui){
            $( "#repuesto" ).attr('disabled','true');
        }
    });

    $("#inventario").autocomplete({
      source: 'http://localhost:8000/searchRepuesto'
  });




} );

$("#boton_reset_repuesto").click(function (e) {
    e.preventDefault();    
    
    $( "#repuesto" ).val('');
    $( "#repuesto" ).prop("disabled", false);
});

$( 
    function show_date(){

        var Digital=new Date();
        var hora=Digital.getHours();
        var minutos=Digital.getMinutes();
        var segundos=Digital.getSeconds();
        var dia = Digital.getDate();
        var mes = Digital.getMonth()+1;
        var año = Digital.getFullYear();
        

        if(hora<=9)
            hora="0"+hora;
        if (minutos<=9)
            minutos="0"+minutos;
        if (mes<=9)
            mes = "0"+mes;
        if (dia<=9)
            dia = "0"+dia;
        
        $('#fecha_movimiento').val(año+"-"+mes+"-"+dia);        

        $('#fecha_entrada').val(año+"-"+mes+"-"+dia);
        $('#fecha_salida').val(año+"-"+mes+"-"+dia);
        
        $('#hora_entrada').val(hora+":"+minutos);
        $('#hora_salida').val(hora+":"+minutos);


    }


    );



